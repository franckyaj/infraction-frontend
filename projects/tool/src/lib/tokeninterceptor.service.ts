import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToolService } from './tool.service';



@Injectable({
  providedIn: 'root'
})
export class TokeninterceptorService implements HttpInterceptor{

  constructor(
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    
    let request = req;
    let config = JSON.parse(localStorage.getItem('config'));
    
    if(req.url.indexOf('authorization-service/oauth/token') != -1) {
      let auth = btoa(config.client_id + ":" +config.client_secret)
          request = req.clone({
      setHeaders: { 
        'Authorization': `Basic ${auth}`,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    });

    }

    if(
      (req.url.indexOf('authorization-service/oauth/token') == -1)
      &&
      (req.url.indexOf('assets/config/app-config.json') == -1)

    ) {
      let jwt = JSON.parse(localStorage.getItem('token')).access_token;
          request = req.clone({
      setHeaders: { 
        'Authorization': `Bearer ${jwt}`,
        'Content-Type': 'application/json'
      }
    });

    }

    return next.handle(request);
  }
}
