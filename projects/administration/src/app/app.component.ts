import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { ToolService } from 'projects/tool/src/public-api';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'administration';

  items: MenuItem[];
  loading = false;
  username: String;


  constructor(
    private router: Router,
    private toolService: ToolService,
    private dialog: MatDialog,
    private permission: NgxPermissionsService
  ) { 
    console.log('in module admin contruct')

  }

  ngOnInit() {
    this.loadPermission();
    this.username = JSON.parse(localStorage.getItem('user')).firstName.toUpperCase() + ' '+ JSON.parse(localStorage.getItem('user')).lastName;
    this.items = [
      {label: 'Quitter', icon: 'pi pi-times', routerLink: ['/main/home']},
      {separator: true},
      {label: 'Se deconnecter', icon: 'pi pi-sign-out', command: () =>{
        this.logout()
      }},
  ];
  }

  loadPermission() {
    let perm = localStorage.getItem('permission');
    console.log('----------------------------------')
    console.log(perm);
    for(let i of perm) {
      this.permission.addPermission(i);
    }
  }

  logout() {
    this.permission.flushPermissions();
    localStorage.clear();
    this.dialog.closeAll();
    this.router.navigate([`/login`]);
    location.reload();
  }
}
