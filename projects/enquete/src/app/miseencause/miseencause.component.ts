import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { MiseEnCause } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-miseencause',
  templateUrl: './miseencause.component.html',
  styleUrls: ['./miseencause.component.css'],
  providers: [MessageService]
})
export class MiseencauseComponent implements OnInit {

  miseencause: MiseEnCause;
  miseencauses: MiseEnCause[];
  infractions = [];
  cuti: any;
  type: any;

  typeSexe= [
    { code: 'M', label: 'Masculin' },
    { code: 'F', label: 'Feminin' },
  ];
  filterNoms: Observable<any[]>;
  myControl = new FormControl();
  infractionCommise: any;

  gav = [
    { code: '1', label: 'Moins de 48h' },
    { code: '2', label: 'Plus de 48h' },
    { code: '3', label: 'Aucune garde à vue' }
  ];

  implication = [
    { code: 'auteur', label: 'Auteur' },
    { code: 'coauteur', label: 'Co-auteur' },
    { code: 'complice', label: 'Complice' },
    { code: 'temoin', label: 'Témoin' },
  ];

  situation = [
    { code: 'defere', label: 'Déféré' },
    { code: 'libéré', label: 'Non déféré' },
    { code: 'enfuite', label: 'En fuite' }
  ];

  etat = [
    { code: 'vivant', label: 'Vivant' },
    { code: 'decede', label: 'Décédé' }
  ]

  nationality = [
    { code: true, label: 'Camerounais' },
    { code: false, label: 'Etranger' },
  ]

  constructor(
    public dialogRef: MatDialogRef<MiseencauseComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: EnqueteService,
    private messageService: MessageService
  ) { }

  loadAll() {
    this.miseencause = new MiseEnCause();
    console.log(this.data);
  
    if(this.data.type != null) this.type = this.data.type;
   // this.infractions = JSON.parse(localStorage.getItem('infractions'));
    if(this.data.data != null) {
      this.miseencause = this.data.data;
    this.miseencause.alias = this.data.data.alias;
    this.miseencause.id = this.data.data.mecId;
    this.miseencause.telephone = this.data.data.telephone;
    this.miseencause.lieuNaissance = this.data.data.lieuNaissance;
    this.miseencause.numeroCni = this.data.data.numCni;
    this.miseencause.cameroonian = this.data.data.nationalite;
    }

    this.miseencause.infractionCommises = [];
    this.miseencause.infractionCommises.push(JSON.parse(localStorage.getItem('mec_infraction')));
   // alert(JSON.parse(localStorage.getItem('mec_infraction')))
    this.infractionCommise = JSON.parse(localStorage.getItem('mec_infraction'));

    this.miseencauses = [];
    this.filterNoms = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => name ? this._filter(name) : this.data.obj.slice())
    );
  }

  displayFn(name: any): string {
    return name ? name : '';
  }
  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.data.obj.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit(): void {
    this.loadAll();

    this.miseencauses = [];
    this.cuti = JSON.parse(localStorage.getItem('user')).login;

  }

  cancel() {
    this.miseencause = new MiseEnCause();
    this.miseencauses = [];
    this.dialogRef.close();
  }

  addNew() {
    this.miseencause.dateCreation = new Date();
    this.miseencause.cuti = this.cuti;
    this.miseencauses.push(this.miseencause);
    this.miseencause = new MiseEnCause();
    console.log(this.miseencauses);
  }

  save() {
    this.miseencause.nom = this.miseencause.nom.toUpperCase();
    if(this.miseencause.prenom == null) this.miseencause.prenom = '';
    this.miseencause.prenom = this.miseencause.prenom.toUpperCase();
    this.miseencause.dateCreation = new Date();
    this.miseencause.cuti = this.cuti;
    this.miseencauses.push(this.miseencause);
    
    this.dialogRef.close(this.miseencauses)

   /* let obj = {
      miseEnCauses: this.miseencauses
    }
    this.service.add(obj, '/miseencause/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )*/
  }

  edit() {
    this.miseencause.nom = this.miseencause.nom.toUpperCase();
    if(this.miseencause.prenom == null) this.miseencause.prenom = '';
    this.miseencause.prenom = this.miseencause.prenom.toUpperCase();
    this.miseencause.dateCreation = new Date();
    this.miseencause.cuti = this.cuti;
    this.miseencauses.push(this.miseencause);
    
    this.dialogRef.close(this.miseencauses)

    let obj = {
      miseEnCauses: this.miseencauses
    }
    this.service.add(obj, '/miseencause/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )
  }
  onSaveSuccess(res) {
    this.dialogRef.close(res)
  }


  onDelete(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:46000, detail: 'Opération effectuée avec succès!!!'});
    this.dialogRef.close(res);

  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  //save victime
  saveEntity() {

  }

  closeDialog() {
    this.dialogRef.close(this.miseencauses)
  }

  delete() {
    this.service.delete('/miseencause/delete/'+this.data.data.id)
    .subscribe(
      (res) => this.onDelete(res),
      (error) => this.onError(error)
    )
  }

}
