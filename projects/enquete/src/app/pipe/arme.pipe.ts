import { Pipe, PipeTransform } from '@angular/core';
import { Arme } from '../model/enquete';

@Pipe({
  name: 'arme'
})
export class ArmePipe implements PipeTransform {

  transform(value: Arme[], ...args: unknown[]): unknown {
    console.log('------PIPE---------');
    console.log(value)
    let res = '';
    for(let i of value) res = res + i.type + ';' 
    console.log(res);
    console.log('------PIPE---------');
    return res;
  }

}
