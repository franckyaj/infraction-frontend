import { Component, Input, OnInit } from '@angular/core';
import { MiseEnCause } from 'projects/administration/src/app/model/enquete';

@Component({
  selector: 'app-libelle',
  templateUrl: './libelle.component.html',
  styleUrls: ['./libelle.component.css']
})
export class LibelleComponent implements OnInit {

  @Input()
  miseencauses: MiseEnCause[];

  @Input()
  type: any;

  constructor() { }

  ngOnInit(): void {
  }

}
