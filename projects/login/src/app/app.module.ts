import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { ToolModule } from 'projects/tool/src/public-api';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxLoadingModule } from 'ngx-loading';
import { GavpipePipe } from './pipe/gavpipe.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GavpipePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToolModule,
    NgxLoadingModule.forRoot({}),
    NgxPermissionsModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
