import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToolService } from 'projects/tool/src/public-api';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  constructor(
    private http: HttpClient,
    private toolService: ToolService
  ) { }

  listProfil() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.get(this.toolService.address + '/' + this.toolService.adminPath + '/role/list')
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  addProfil(obj) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.adminPath + '/role/add', obj)
    .pipe(map((res: any) => {
      return res;
      } ));
  }
}
