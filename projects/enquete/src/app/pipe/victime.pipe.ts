import { Pipe, PipeTransform } from '@angular/core';
import { Enquete, Victime } from '../model/enquete';

@Pipe({
  name: 'victime'
})
export class VictimePipe implements PipeTransform {

  transform(value: Enquete, ...args: unknown[]): unknown {
    let mecs = value.victimes;
    let res = '';
    for(let item of mecs) res = res + item.nom + ' ' + item.prenom + '; '
    return res;

  }
}
