import { Component, OnInit } from '@angular/core';
import { Arme, Enquete, Infraction, MiseEnCause, Stupefiant, VehiculeVole, Victime } from '../model/enquete';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Locality } from '../model/locality';
import { MatDialog } from '@angular/material/dialog';
import { VictimeComponent } from '../victime/victime.component';
import { MiseencauseComponent } from '../miseencause/miseencause.component';
import { StupefiantComponent } from '../stupefiant/stupefiant.component';
import { VehiculevoleComponent } from '../vehiculevole/vehiculevole.component';
import { ArmeComponent } from '../arme/arme.component';
import { EnqueteService } from '../service/enquete.service';
import { subscribeOn } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxPermissionsService } from 'ngx-permissions';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MessageService]
})
export class HomeComponent implements OnInit {

  enquete: Enquete;
  locality: Locality;
  victimes: Victime[];
  stupefiants: Stupefiant[];
  armes: Arme[];
  vehicules: VehiculeVole[];
  miseencauses: MiseEnCause[];
  infraction: Infraction;
  infractions: Infraction[];
  isError = '2';
  typeArme: any;
  natureStupefiant: any;
  marqueVehicule: any;
  personne: any;
  proprietaireisVictime = false;
  response: any;
  delegation: any;
  infractionCommises: any;
  cuti: any;

  typeSaisie = [
    {code: 'plainte', label: 'Plainte'},
    {code: 'intruction_parquet', label: 'Instruction du parquet'},
    {code: 'comission_derogatoire', label: 'Comission dérogatoire'},
    {code: 'denonciation', label: 'Dénonciation'},
    {code: 'initiative_service', label: 'Initiaive service'},
    {code: 'reseignement_judiciaire', label: 'Renseignement judiciaire'},

  ];

  cadreJuridique= [
    {code: 'enquete_preliminaire', label: 'Enquete préliminaire'},
    {code: 'flag_delit', label: 'Flagrant délit'},
    {code: 'comission_regatoire', label: 'Comission rogatoire'}
  ];

  categorieInfraction = [
    {code: '01', label: 'ATTEINTES A L\'AUTORITE PUBLIQUE'},
    {code: '02', label: 'ATTEINTES AU GARANTIES DE L\'ETAT'},
    {code: '03', label: 'ATTEINTES A LA SECURITE PUBLIQUE'},
    {code: '04', label: 'ATTEINTES A LA PAIX PUBLIQUE'},
    {code: '05', label: 'ATTEINTES A L\'ECONOMIE PUBLIQUE'},
    {code: '06', label: 'ATTEINTES A LA SANTE PUBLIQUE'},
    {code: '07', label: 'ATTEINTES A LA MORALITE PUBLIQUE'},
    {code: '08', label: 'ATTEINTES AUX CULTES'},
    {code: '09', label: 'ATTEINTES A L\'INTEGRITE CORPORELLE'},
    {code: '10', label: 'ATTEINTES A LA LIBERTE ET A LA PAIX DES PERSONNES'},
    {code: '11', label: 'ATTEINTES A LA CONFIANCE DES PERSONNES'},
    {code: '12', label: 'ATTEINTE CONTRE L\'ENFANT ET LA FAMILLE'},
    {code: '13', label: 'ATTEINTES A LA SURETE DE L\'ETAT'},
    {code: '14', label: 'ATTEINTES A LA CONSTITUTION'},
    {code: '15', label: 'INFRATIONS COMMISE PAR LES FONCTIONNAIRES'},
  ];

  isEditable = false;
  filterUnites: Observable<any[]>;
  regions = [];
  myControl = new FormControl();
  departements = [];
  arrondissements = [];
  delegations = [];
  unites = [];

  mockDaats = {
    "lieu": "cdcdsc",
    "id":null,
    "referenceProcedure": "cdcds",
    "parquetCompetant": "cdcsdcs",
    "affaire": "cdscdsc",
    "cadreJuridique": "flag_delit",
    "coordonneesOpj": "cscqs",
    "coordonneesAssistant": "fezfefez",
    "resumeFaits": "cdscds",
    "armes":null,
    "stupefiants":null,
    "victimes": [
      {
        "cuti": "YAJE",
        "id": 3575,
        "nom": "LOLO",
        "prenom": "",
        "dateNaissance": new Date(),
        "dateCreation": new Date(),
        "lieuNaissance": null,
        "sexe": null,
        "nationalite": null,
        "autreNationalite": null,
        "etat": "decede",
        "numeroCni": null,
        "adresse": null,
        "telephone": null,
        "cameroonian": true,
        "active": true
      }
    ],
    "miseencauses": [
      {
        "cuti": "YAJE",
        "id": 3577,
        "nom": "LOLO",
        "prenom": "",
        "dateNaissance": null,
        "dateCreation": new Date(),
        "lieuNaissance": null,
        "sexe": "M",
        "nationalite": null,
        "autreNationalite": null,
        "etat": "vivant",
        "numeroCni": null,
        "adresse": null,
        "telephone": null,
        "cameroonian": true,
        "active": true,
        "alias": null,
        "gav": "1",
        "implication": "coauteur",
        "situation": "defere",
        "infractionCommises": [
          "ABUS DE CONFIANCE ET ESCROQUERIE AGGRAVES"
        ]
      }
    ],
    "vehiculeVoles": [
      {
        "id": 3579,
        "marque": "test",
        "immatriculation": "cdscd",
        "chasis": "cdscs",
        "etat": "recherche",
        "dateCreation": null,
        "proprietaire": "cdscd",
        "cuti": "YAJE",
        "active": true
      }
    ],
    "cuti": "YAJE",
    "infraction": {
      "infractionCommises": [
        "ABUS DE CONFIANCE ET ESCROQUERIE AGGRAVES"
      ],
      "id":null,
      "active":true,
      "categorie": "ATTEINTES A LA CONFIANCE DES PERSONNES",
      "typeSaisie": "initiative_service",
      "rreferenceSaisie": "csccs",
      "date": new Date(),
      "adressePrecise": "cdsdscd",
      "localite": {
        "region": "EST",
        "departement": "HAUT NYONG ",
        "arrondissement": "BOMA",
        "delegation": "Délégation Régionale de la Sûreté Nationale de l’Est",
        "unite": "Commissariat de Sécurité Publique de BATOURI"
      },
      "cuti": "YAJE"
    },
    "dateCreation": new Date(),
    "active": true,
  };




  constructor(
    private dialog: MatDialog,
    private service: EnqueteService,
    private permission: NgxPermissionsService,
    private spinner: NgxSpinnerService,
    private messageService: MessageService
  ) { }

  loadDatas() {
    this.spinner.show();
    this.service.filterTool()
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  onLoadSuccess(res) {
    this.spinner.hide();
    this.typeArme = res.types;
    this.natureStupefiant = res.natures;
    this.marqueVehicule = res.marques;
    this.personne = res.noms;
    this.regions = res.regions;
    this.infractions = res.categories;

    if(this.regions == [] ) {
      this.messageService.add({severity:'error', summary: 'Attention',life:40000, detail: 'Lees reegion ne sont pas disponible!!'});
    }
    if(this.infractions == [] ) {
      this.messageService.add({severity:'error', summary: 'Attention',life:40000, detail: 'Lees reegion ne sont pas disponible!!'});
    }
    localStorage.setItem('regions', JSON.stringify(this.regions))
  }

  onError(error) {
    this.spinner.hide();
    this.isError = '1';
    this.messageService.add({severity:'error', summary: 'Attention',life:4000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }
  
  loadAll(){
    this.enquete = new Enquete();
    this.locality = new Locality();
    this.victimes = [];
    this.miseencauses = [];
    this.vehicules = [];
    this.armes = [];
    this.stupefiants = [];
    this.infraction = new Infraction();
    this.cuti = JSON.parse(localStorage.getItem('user')).login;
    this.loadDatas();
    
  }

  loadFilter() {
    this.filterUnites = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => name ? this._filter(name) : this.regions.slice())
    );

  }

  ngOnInit(): void {
    this.loadAll();
    this.loadFilter();
    
  }

  addOPJ() {

  }

  addAssistant() {

  }

  addMiseEnCause() {
    localStorage.setItem('infractions',JSON.stringify(this.infraction.infractionCommises))
    let dialogRef = this.dialog.open(MiseencauseComponent, {
      disableClose: false,
      width: '55%',
      data: {obj: this.personne, data: null}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != undefined && result != null) {
          this.miseencauses = this.miseencauses.concat(result);
          this.enquete.miseencauses = this.miseencauses;
        }
      }
    )
  }

  addStupefiant() {
    let dialogRef = this.dialog.open(StupefiantComponent, {
      disableClose: false,
      width: '35%',
      data: {obj: this.natureStupefiant, data:null}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != undefined && result != null) {
          this.stupefiants = this.stupefiants.concat(result);
          this.enquete.stupefiants = this.stupefiants;
        }
      }
    )
  }

  addVehiculeVole() {
    let dialogRef = this.dialog.open(VehiculevoleComponent, {
      disableClose: false,
      width: '35%',
      data: {obj:this.marqueVehicule, data:null}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != undefined && result != null) {
          this.vehicules = this.vehicules.concat(result);
          this.enquete.vehiculeVoles = this.vehicules;
        }
      }
    )
  }


  addArme() {
    let dialogRef = this.dialog.open(ArmeComponent, {
      disableClose: false,
      width: '35%',
      data: {obj:this.typeArme, data:null}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != undefined && result != null) {
          this.armes = this.armes.concat(result);
          this.enquete.armes = this.armes;
        }
      }
    )
  }

  addVictime() {
    let dialogRef = this.dialog.open(VictimeComponent, {
      disableClose: false,
      width: '35%',
      data: {obj:this.personne, data:null}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != undefined && result != null) {
          this.victimes = this.victimes.concat(result);
          this.enquete.victimes = this.victimes;
        }
      }
    )
  }

  save() {
    this.spinner.show();
    this.enquete.cuti = this.cuti;
    this.infraction.localite = this.locality;
    this.infraction.cuti = this.cuti;
    this.enquete.infraction = this.infraction;
    this.enquete.dateCreation = new Date();
    this.service.add(this.enquete, '/enquete/save')
    .subscribe(
      (res) => this.onAddSuccess(res),
      (error) => this.onError(error)
    )
  }

  onAddSuccess(res) {
    this.spinner.hide();
   if(res != null) {
    this.response = res;
    this.isError = '0';
    this.initForm();
    this.messageService.add({severity:'success', summary: 'Information',life:6000, detail: 'Opération effectuée avec succès!!'});
   }
   else {
    this.isError = '1';
    this.messageService.add({severity:'error', summary: 'Attention',life:6000, detail: 'Une enquete existe deja avec les references saisies!!'});
   }
  }

  initForm() {
    this.enquete = new Enquete();
    this.infraction = new Infraction();
    this.locality = new Locality();
    this.delegations = null;
    this.infractionCommises = null;
    this.enquete.victimes = [];
    this.enquete.miseencauses = [];
    this.enquete.armes = [];
    this.enquete.stupefiants = [];
    this.enquete.vehiculeVoles = [];
    this.miseencauses = [];
    this.victimes = [];
    this.stupefiants = [];
    this.armes = [];
    this.vehicules = [];
  }

  getDepartement(event) {
    this.spinner.show();
    let selectedRegion = event.value;
    this.departements= [];
    this.arrondissements= [];
    this.service.getRegionsDates(selectedRegion, 'departement')
    .subscribe(
      (res) => this.onDeptSuccess(res),
      (error) => this.onError(error)
    )
  }

  getArrondissement(event) {
    let selectedDept = event.value;
    this.arrondissements= [];
    this.service.getRegionsDates(selectedDept, 'arrondissement')
    .subscribe(
      (res) => this.onArrdonsieement(res),
      (error) => this.onError(error)
    )
  }

  getUnite(event) {
    let selectedDept = event.value;
   // this.arrondissements= [];
    this.service.getRegionsDates(selectedDept, 'unite')
    .subscribe(
      (res) => this.onUniteSuccess(res),
      (error) => this.onError(error)
    )
  }

  onUniteSuccess(res) {
    this.unites = res[0];
    this.locality.delegation = this.delegation;
  }

  onDeptSuccess(res) {
    this.spinner.hide();
    this.departements = res[0];
    this.delegations = res[1];
  }
  onArrdonsieement(res) {
    this.spinner.hide();
    this.arrondissements = res[0];
  }


  /******** */
  displayFn(name: any): string {
    return name ? name : '';
  }


  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.regions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  getInfractions(event) {
    this.spinner.show();
    let selectedCategorie = event.value;
    this.infraction.infractionCommises = [];
   // this.infraction.infractionCommises.push(event.value);
    localStorage.setItem('mec_infraction', JSON.stringify(selectedCategorie));
    console.info(JSON.parse(localStorage.getItem('mec_infraction')));
    this.service.getInfractions(selectedCategorie)
    .subscribe(
      (res) => this.onGetInfractions(res),
      (error) => this.onError(error)
    )

  }

  onGetInfractions(res) {
    this.spinner.hide();
    this.infraction.categorie = res[0];
    this.infraction.infractionCommises.push(this.infractionCommises);
  }


  removeMecItem(index) {
    this.miseencauses.splice(index, 1);
  }

  removeVictimeItem(index) {
    this.victimes.splice(index, 1);
  }

  removeVehiculeItem(index) {
    this.vehicules.splice(index, 1);
  }

  removeStupefiantItem(index) {
    this.stupefiants.splice(index, 1);
  }

  removeArmeItem(index) {
    this.armes.splice(index, 1);
  }

  onEnter(){
    this.spinner.show();
    let obj = {
      referenceProcedure: this.enquete.referenceProcedure
    }
    this.service.list(obj, '/enquete/filter')
    .subscribe(
      (res) => this.onFilterSuccess(res),
      (error) => this.onError(error)
    )
  }

  onFilterSuccess(res) {
    this.spinner.hide();
    console.log(res);
    this.enquete.parquetCompetant = res[0].parquetCompetant;
    this.enquete.affaire = res[0].affaire;
    this.enquete.cadreJuridique = res[0].cadreJuridique;
    this.enquete.coordonneesOpj = res[0].parquetCompetant;
    this.enquete.coordonneesAssistant = res[0].coordonneesAssistant;
    this.enquete.resumeFaits = res[0].resumeFaits;


  }

}
