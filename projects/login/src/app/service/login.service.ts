import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ToolService } from 'projects/tool/src/public-api';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private toolService: ToolService,
    private http: HttpClient
  ) { }

  login(loginDto) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.adminPath + '/user/login', loginDto)
    .pipe(map((res: any) => {
      return res;
      } ));
  }


  editPwd(loginDto) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.adminPath + '/user/editpwd', loginDto)
    .pipe(map((res: any) => {
      return res;
      } ));
  }
}
