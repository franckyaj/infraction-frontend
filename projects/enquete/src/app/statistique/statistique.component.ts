import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { SearchDto } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import { ExcelService } from 'projects/tool/src/lib/service/excel.service';
import { PdfService } from 'projects/tool/src/lib/service/pdf.service';
import { groupBy } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-statistique',
  templateUrl: './statistique.component.html',
  styleUrls: ['./statistique.component.css'],
  providers: [MessageService]
})
export class StatistiqueComponent implements OnInit {

  stats = [
    {code: 'enquete', label: 'Enquete'},
    {code: 'infraction', label: 'Infraction'}
  ];

  title: any;
  dataForExcel = [];
  criteria = [];

  criteriaEnquete = [
    {code: 'region', label: 'Régions'},
    {code: 'dept', label: 'Département'},
    {code: 'arr', label: 'Arrondissement'},
    {code: 'del', label: 'Delegation regionale'},
    {code: 'unit', label: 'Unité'},
  ];

  criteriaInfraction = [
    {code: 'region', label: 'Régions'},
    {code: 'dept', label: 'Département'},
    {code: 'arr', label: 'Arrondissement'},
    {code: 'del', label: 'Delegation regionale'},
    {code: 'unit', label: 'Unité'},
    {code: 'categorie', label: 'Categoriee infraction'},
  ];
  
  seachDto: SearchDto;
  datas: any[];

  type: any;
  critere: any;
  ttile_stat: any;

  today = new Date();
  month = this.today.getMonth();
  year =   this.today.getFullYear();

  constructor(
    private service: EnqueteService,
    private  datePipe: DatePipe,
    private exportExcsk: ExcelService,
    private messageService: MessageService,
    private pdfService: PdfService,
    private spinner: NgxSpinnerService
  ) { }

  getCriteeria(event) {
    if(event.value == 'enquete') {
      this.criteria = this.criteriaEnquete
    }
    else {
      this.criteria = this.criteriaInfraction
    }
  }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(){
    this.datas = [];
    this.seachDto = new SearchDto();
    this.seachDto.debut = new Date(this.year, this.month,1);
    this.seachDto.fin = new Date(this.year, this.month,this.today.getDate());
  }
//    this.seachDto.fin =  this.getTomorrow();// new Date(this.year, this.month,this.today.getDate());

  loadDatas() {
    this.spinner.show();
    let obj = {};
    this.seachDto.fin =  this.getTomorrow();// new Date(this.year, this.month,this.today.getDate());
    this.datas = [];
    if(this.seachDto.referenceProcedureLike == '') this.seachDto.referenceProcedureLike = null;
    let paht = '';
    if(this.type == 'enquete') paht = '/enquete/stats1';
    if(this.type == 'infraction') paht = '/enquete/stats2';

    this.service.list(this.seachDto, paht)
    .subscribe(
      (res) => this.onFilterSuccess(res),
      (error) => this.onError(error)
    )
  }

  onFilterSuccess(res) {
    this.spinner.hide();
    this.datas = res;
    let rows = [];
    if(this.type == 'enquete') this.enqueteStat(rows, res)
    if(this.type == 'infraction') this.infractionStat(rows, res)
    this.seachDto.fin = new Date(this.year, this.month,this.today.getDate());
  }

  enqueteStat(rows, res) {
    this.ttile_stat = 'STATISTIQUE DES ENQUETES ';
    if(this.critere == 'region') rows = this.contructObjectDatasByRegion(res);
    if(this.critere == 'dept') rows = this.contructObjectDatasByDept(res);
    if(this.critere == 'arr') rows = this.contructObjectDatasByArr(res);
    if(this.critere == 'del') rows = this.contructObjectDatasByDel(res);
    if(this.critere == 'unit') rows = this.contructObjectDatasByUnit(res);


  }

  infractionStat(rows, res) {
    this.ttile_stat = 'STATISTIQUE DES INFRACTIONS ';
    if(this.critere == 'categorie') rows = this.contructObjectInfractionByCat(res);
    if(this.critere == 'unit') rows = this.contructObjectInfractionByUnit(res);
    if(this.critere == 'del') rows = this.contructObjectInfractionByDel(res);
    if(this.critere == 'arr') rows = this.contructObjectInfractionByArr(res);
    if(this.critere == 'dept') rows = this.contructObjectInfractionByDept(res);
    if(this.critere == 'region') rows = this.contructObjectInfractionByRegion(res);

  }

  onError(error) {
    this.spinner.hide();
    this.seachDto.fin = new Date(this.year, this.month,this.today.getDate());
    this.messageService.add({severity:'error', summary: 'Attention',life:4600, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }


  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
       const key = keyGetter(item);
       const collection = map.get(key);
       if (!collection) {
          map.set(key, [item]);
       } else {
          collection.push(item);
       }
    });
    return map;
 }

 groupByList(list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
     const key = keyGetter(item);
     const collection = map.get(key);
     if (!collection) {
        map.set(key, [item]);
     } else {
        collection.push(item);
     }
  });
  return map;
}


 contructObjectDatasByRegion(datas) {
   this.title = 'stats_enquete';
   this.ttile_stat = this.ttile_stat + 'PAR REGION'
   let subgrouped = null;
   let subgroupeds = [];
   let nb_initiative_service = 0
   let nbplainte = 0;
   let nb_comission_derogatoire = 0;
   let nb_denonciation = 0;
   let nb_intruction_parquet = 0
  const grouped = this.groupBy(datas, i => i.region);
  for (let [key, value] of grouped) {
     subgrouped = this.groupBy(value, i => i.typeSaisie);
     for (let [subkey, subvalue] of subgrouped) {
       if(subkey == 'initiative_service') nb_initiative_service = subvalue.length;
       if(subkey == 'plainte') nbplainte = subvalue.length;
       if(subkey == 'intruction_parquet') nb_intruction_parquet = subvalue.length;
       if(subkey == 'denonciation') nb_denonciation = subvalue.length;
       if(subkey == 'comission_derogatoire') nb_comission_derogatoire = subvalue.length;
     }
     subgroupeds.push(
       {
         'region': key,
         'plainte': nbplainte,
         'initiative_service':nb_initiative_service,
         'intruction_parquet':nb_intruction_parquet,
         'denonciation': nb_denonciation,
         'comission_derogatoire':nb_comission_derogatoire,
         'total': nbplainte + nb_initiative_service + nb_intruction_parquet + nb_denonciation +nb_comission_derogatoire
       }
     );
     nb_initiative_service = 0
     nbplainte = 0;
     nb_comission_derogatoire = 0;
     nb_denonciation = 0;
     nb_intruction_parquet = 0
  }
  this.title = 'stats_enquete_region';
  this.exportPdf(subgroupeds, 'region', this.ttile_stat);
  return subgroupeds;
 }


 contructObjectDatasByDept(datas) {
  this.title = 'stats_enquete';
  this.ttile_stat = this.ttile_stat + 'PAR DEPARTEMENT'

  let subgrouped = null;
  let subgroupeds = [];
  let nb_initiative_service = 0
  let nbplainte = 0;
  let nb_comission_derogatoire = 0;
  let nb_denonciation = 0;
  let nb_intruction_parquet = 0
 const grouped = this.groupBy(datas, i => i.departement);
 for (let [key, value] of grouped) {
    subgrouped = this.groupBy(value, i => i.typeSaisie);
    for (let [subkey, subvalue] of subgrouped) {
      if(subkey == 'initiative_service') nb_initiative_service = subvalue.length;
      if(subkey == 'plainte') nbplainte = subvalue.length;
      if(subkey == 'intruction_parquet') nb_intruction_parquet = subvalue.length;
      if(subkey == 'denonciation') nb_denonciation = subvalue.length;
      if(subkey == 'comission_derogatoire') nb_comission_derogatoire = subvalue.length;
    }
    subgroupeds.push(
      {
        'dept': key,
        'plainte': nbplainte,
        'initiative_service':nb_initiative_service,
        'intruction_parquet':nb_intruction_parquet,
        'denonciation': nb_denonciation,
        'comission_derogatoire':nb_comission_derogatoire,
        'total': nbplainte + nb_initiative_service + nb_intruction_parquet + nb_denonciation +nb_comission_derogatoire
      }
    );
    nb_initiative_service = 0
    nbplainte = 0;
    nb_comission_derogatoire = 0;
    nb_denonciation = 0;
    nb_intruction_parquet = 0
 }
 this.title = 'stats_enquete_dept';
 this.exportPdf(subgroupeds, 'departement', this.ttile_stat);
 return subgroupeds;
}


contructObjectDatasByArr(datas) {
  this.title = 'stats_enquete';
  this.ttile_stat = this.ttile_stat + 'PAR ARRONDISSEMENT'

  let subgrouped = null;
  let subgroupeds = [];
  let nb_initiative_service = 0
  let nbplainte = 0;
  let nb_comission_derogatoire = 0;
  let nb_denonciation = 0;
  let nb_intruction_parquet = 0
 const grouped = this.groupBy(datas, i => i.arrondissement);
 for (let [key, value] of grouped) {
    subgrouped = this.groupBy(value, i => i.typeSaisie);
    for (let [subkey, subvalue] of subgrouped) {
      if(subkey == 'initiative_service') nb_initiative_service = subvalue.length;
      if(subkey == 'plainte') nbplainte = subvalue.length;
      if(subkey == 'intruction_parquet') nb_intruction_parquet = subvalue.length;
      if(subkey == 'denonciation') nb_denonciation = subvalue.length;
      if(subkey == 'comission_derogatoire') nb_comission_derogatoire = subvalue.length;
    }
    subgroupeds.push(
      {
        'arr': key,
        'plainte': nbplainte,
        'initiative_service':nb_initiative_service,
        'intruction_parquet':nb_intruction_parquet,
        'denonciation': nb_denonciation,
        'comission_derogatoire':nb_comission_derogatoire,
        'total': nbplainte + nb_initiative_service + nb_intruction_parquet + nb_denonciation +nb_comission_derogatoire
      }
    );
    nb_initiative_service = 0
    nbplainte = 0;
    nb_comission_derogatoire = 0;
    nb_denonciation = 0;
    nb_intruction_parquet = 0
 }
 this.title = 'stats_enquete_arr';
 this.exportPdf(subgroupeds, 'arrondissement', this.ttile_stat);
 return subgroupeds;
}

contructObjectInfractionByUnit(datas) {
  this.title = 'stats_infraction_unit';
  let subgroupedSexe = null;
  let subgroupedGav = null;
  let subgroupedMaj = null;
  let subgroupedNat = null;
  let subgroupedSituation = null;
  let subgroupeds = [];
  let nb_p48gav = 0
  let nb_m48gav = 0;
  let nb_nogav = 0;
  let nb_libere = 0;
  let nb_defere = 0;
  let nb_fuite = 0;
  let nb_cam = 0;
  let nb_etranger = 0;
  let nb_majF = 0;
  let nb_majM = 0;
  let nb_minF = 0;
  let nb_minM = 0;
  let nb_masc = 0;
  let nb_fem = 0;
  let x_majF = 0;
  let x_majM = 0;
 const grouped = this.groupByList(datas, i => i.unite);
 console.log(grouped);
 for (let [key, value] of grouped) {
  subgroupedSexe = this.groupBy(value, i => i.sexe);
  subgroupedGav = this.groupBy(value, i => i.gav);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedNat = this.groupBy(value, i => i.camer);

  for (let [subkey, subvalue] of subgroupedSexe) {
    subgroupedMaj = this.groupBy(value, i => i.majeur);
    for (let [subkey0, subvalue0] of subgroupedMaj) {
      if(subkey0 == 0 && subkey == 'M') nb_minM =  subvalue0.length;
      if(subkey0 == 0 && subkey == 'F') nb_minF =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'M') nb_majM =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'F') nb_majF =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'M') x_majM =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'F') x_majF =  subvalue0.length;
    }
  }
  for (let [subkey, subvalue] of subgroupedGav) {
    if(subkey == '1') nb_m48gav = subvalue.length;
    if(subkey == '2') nb_p48gav = subvalue.length;
    if(subkey == '3') nb_nogav = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedNat) {
    if(subkey == true) nb_cam = subvalue.length;
    if(subkey == false) nb_etranger = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedSituation) {
    if(subkey == '1defere') nb_libere = subvalue.length;
    if(subkey == 'nondefere') nb_defere = subvalue.length;
    if(subkey == 'enfuite') nb_fuite = subvalue.length;
  }

  subgroupeds.push(
    {
      'unite': key,
      'fait': 0,
      'mgav': nb_m48gav,
      'pgav':nb_p48gav,
      'gav': nb_nogav,
      'libere': nb_libere,
      'defere':nb_defere,
      'enfuite': nb_fuite,
      'camer': nb_cam,
      'etr': nb_etranger,
      'fMin': nb_minF,
      'mMiM': nb_minM,
      'majF': nb_majF,
      'majM': nb_majM,
      'xmajF': x_majF,
      'xmajM': x_majM
    }
  );
  nb_p48gav = 0
  nb_m48gav = 0;
  nb_libere = 0;
  nb_defere = 0;
  x_majF = 0;
  x_majM = 0;
  nb_fuite = 0;
  nb_cam = 0;
  nb_nogav = 0;
  nb_etranger = 0;
  nb_majF = 0;
  nb_majM = 0;
  nb_minF = 0;
  nb_minM = 0;
  nb_masc = 0;
  nb_fem = 0;
 }
 this.title = 'stats_infraction';
 this.exportPdf(subgroupeds, 'infraction_unit', this.ttile_stat);
 return subgroupeds;
}

contructObjectInfractionByDel(datas) {
  this.title = 'stats_infraction_del';
  let subgroupedSexe = null;
  let subgroupedGav = null;
  let subgroupedMaj = null;
  let subgroupedNat = null;
  let subgroupedSituation = null;
  let subgroupeds = [];
  let nb_p48gav = 0
  let nb_m48gav = 0;
  let nb_nogav = 0;
  let nb_libere = 0;
  let nb_defere = 0;
  let nb_fuite = 0;
  let nb_cam = 0;
  let nb_etranger = 0;
  let nb_majF = 0;
  let nb_majM = 0;
  let nb_minF = 0;
  let nb_minM = 0;
  let nb_masc = 0;
  let nb_fem = 0;
  let x_majF = 0;
  let x_majM = 0;
 const grouped = this.groupByList(datas, i => i.delegation);
 for (let [key, value] of grouped) {
  subgroupedSexe = this.groupBy(value, i => i.sexe);
  subgroupedGav = this.groupBy(value, i => i.gav);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedNat = this.groupBy(value, i => i.camer);

  for (let [subkey, subvalue] of subgroupedSexe) {
    subgroupedMaj = this.groupBy(value, i => i.majeur);
    for (let [subkey0, subvalue0] of subgroupedMaj) {
      if(subkey0 == 0 && subkey == 'M') nb_minM =  subvalue0.length;
      if(subkey0 == 0 && subkey == 'F') nb_minF =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'M') nb_majM =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'F') nb_majF =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'M') x_majM =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'F') x_majF =  subvalue0.length;
    }
  }
  for (let [subkey, subvalue] of subgroupedGav) {
    if(subkey == '1') nb_m48gav = subvalue.length;
    if(subkey == '2') nb_p48gav = subvalue.length;
    if(subkey == '3') nb_nogav = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedNat) {
    if(subkey == true) nb_cam = subvalue.length;
    if(subkey == false) nb_etranger = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedSituation) {
    if(subkey == '1defere') nb_libere = subvalue.length;
    if(subkey == 'nondefere') nb_defere = subvalue.length;
    if(subkey == 'enfuite') nb_fuite = subvalue.length;
  }

  subgroupeds.push(
    {
      'del': key,
      'fait': 0,
      'mgav': nb_m48gav,
      'pgav':nb_p48gav,
      'gav': nb_nogav,
      'libere': nb_libere,
      'defere':nb_defere,
      'enfuite': nb_fuite,
      'camer': nb_cam,
      'etr': nb_etranger,
      'fMin': nb_minF,
      'mMiM': nb_minM,
      'majF': nb_majF,
      'majM': nb_majM,
      'xmajF': x_majF,
      'xmajM': x_majM
    }
  );
  nb_p48gav = 0
  nb_m48gav = 0;
  nb_libere = 0;
  nb_defere = 0;
  nb_fuite = 0;
  nb_cam = 0;
  nb_nogav = 0;
  nb_etranger = 0;
  nb_majF = 0;
  nb_majM = 0;
  x_majF= 0;
  x_majM = 0;
  nb_minF = 0;
  nb_minM = 0;
  nb_masc = 0;
  nb_fem = 0;
 }
 this.title = 'stats_infraction';
 this.exportPdf(subgroupeds, 'infraction_del', this.ttile_stat);
 return subgroupeds;
}

contructObjectInfractionByArr(datas) {
  this.title = 'stats_infraction_arr';
  let subgroupedSexe = null;
  let subgroupedGav = null;
  let subgroupedMaj = null;
  let subgroupedNat = null;
  let subgroupedSituation = null;
  let subgroupeds = [];
  let nb_p48gav = 0
  let nb_m48gav = 0;
  let nb_nogav = 0;
  let nb_libere = 0;
  let nb_defere = 0;
  let nb_fuite = 0;
  let nb_cam = 0;
  let nb_etranger = 0;
  let nb_majF = 0;
  let nb_majM = 0;
  let nb_minF = 0;
  let nb_minM = 0;
  let nb_masc = 0;
  let x_majF = 0;
  let x_majM = 0;
  let nb_fem = 0;
 const grouped = this.groupByList(datas, i => i.arrondissemeent);
 for (let [key, value] of grouped) {
  subgroupedSexe = this.groupBy(value, i => i.sexe);
  subgroupedGav = this.groupBy(value, i => i.gav);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedNat = this.groupBy(value, i => i.camer);

  for (let [subkey, subvalue] of subgroupedSexe) {
    subgroupedMaj = this.groupBy(value, i => i.majeur);
    for (let [subkey0, subvalue0] of subgroupedMaj) {
      if(subkey0 == 0 && subkey == 'M') nb_minM =  subvalue0.length;
      if(subkey0 == 0 && subkey == 'F') nb_minF =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'M') nb_majM =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'F') nb_majF =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'M') x_majM =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'F') x_majF =  subvalue0.length;
    }
  }
  for (let [subkey, subvalue] of subgroupedGav) {
    if(subkey == '1') nb_m48gav = subvalue.length;
    if(subkey == '2') nb_p48gav = subvalue.length;
    if(subkey == '3') nb_nogav = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedNat) {
    if(subkey == true) nb_cam = subvalue.length;
    if(subkey == false) nb_etranger = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedSituation) {
    if(subkey == '1defere') nb_libere = subvalue.length;
    if(subkey == 'nondefere') nb_defere = subvalue.length;
    if(subkey == 'enfuite') nb_fuite = subvalue.length;
  }

  subgroupeds.push(
    {
      'arr': key,
      'fait': 0,
      'mgav': nb_m48gav,
      'pgav':nb_p48gav,
      'gav': nb_nogav,
      'libere': nb_libere,
      'defere':nb_defere,
      'enfuite': nb_fuite,
      'camer': nb_cam,
      'etr': nb_etranger,
      'fMin': nb_minF,
      'mMiM': nb_minM,
      'majF': nb_majF,
      'majM': nb_majM,
      'xmajF': x_majF,
      'xmajM': x_majM
    }
  );
  nb_p48gav = 0
  nb_m48gav = 0;
  nb_libere = 0;
  x_majF = 0;
  x_majM = 0;
  nb_defere = 0;
  nb_fuite = 0;
  nb_cam = 0;
  nb_nogav = 0;
  nb_etranger = 0;
  nb_majF = 0;
  nb_majM = 0;
  nb_minF = 0;
  nb_minM = 0;
  nb_masc = 0;
  nb_fem = 0;
 }
 this.title = 'stats_infraction';
 this.exportPdf(subgroupeds, 'infraction_arr', this.ttile_stat);
 return subgroupeds;
}

contructObjectInfractionByRegion(datas) {
  this.title = 'stats_infraction_region';
  let subgroupedSexe = null;
  let subgroupedGav = null;
  let subgroupedMaj = null;
  let subgroupedNat = null;
  let subgroupedSituation = null;
  let subgroupeds = [];
  let nb_p48gav = 0
  let nb_m48gav = 0;
  let nb_nogav = 0;
  let nb_libere = 0;
  let nb_defere = 0;
  let nb_fuite = 0;
  let nb_cam = 0;
  let nb_etranger = 0;
  let nb_majF = 0;
  let nb_majM = 0;
  let nb_minF = 0;
  let x_majM = 0;
  let x_majF = 0;
  let nb_minM = 0;
  let nb_masc = 0;
  let nb_fem = 0;
 const grouped = this.groupByList(datas, i => i.region);
 for (let [key, value] of grouped) {
  subgroupedSexe = this.groupBy(value, i => i.sexe);
  subgroupedGav = this.groupBy(value, i => i.gav);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedNat = this.groupBy(value, i => i.camer);

  for (let [subkey, subvalue] of subgroupedSexe) {
    subgroupedMaj = this.groupBy(value, i => i.majeur);
    for (let [subkey0, subvalue0] of subgroupedMaj) {
    if(subkey0 == 0 && subkey == 'M') nb_minM =  subvalue0.length;
    if(subkey0 == 0 && subkey == 'F') nb_minF =  subvalue0.length;
    if(subkey0 == 1 && subkey == 'M') nb_majM =  subvalue0.length;
    if(subkey0 == 1 && subkey == 'F') nb_majF =  subvalue0.length;
    if(subkey0 == 3 && subkey == 'M') x_majM =  subvalue0.length;
    if(subkey0 == 3 && subkey == 'F') x_majF =  subvalue0.length;
    }
  }
  for (let [subkey, subvalue] of subgroupedGav) {
    if(subkey == '1') nb_m48gav = subvalue.length;
    if(subkey == '2') nb_p48gav = subvalue.length;
    if(subkey == '3') nb_nogav = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedNat) {
    if(subkey == true) nb_cam = subvalue.length;
    if(subkey == false) nb_etranger = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedSituation) {
    if(subkey == '1defere') nb_libere = subvalue.length;
    if(subkey == 'nondefere') nb_defere = subvalue.length;
    if(subkey == 'enfuite') nb_fuite = subvalue.length;
  }

  subgroupeds.push(
    {
      'region': key,
      'fait': 0,
      'mgav': nb_m48gav,
      'pgav':nb_p48gav,
      'gav': nb_nogav,
      'libere': nb_libere,
      'defere':nb_defere,
      'enfuite': nb_fuite,
      'camer': nb_cam,
      'etr': nb_etranger,
      'fMin': nb_minF,
      'mMiM': nb_minM,
      'majF': nb_majF,
      'majM': nb_majM
    }
  );
  nb_p48gav = 0
  nb_m48gav = 0;
  nb_libere = 0;
  nb_defere = 0;
  nb_fuite = 0;
  x_majF = 0;
  x_majM = 0;
  nb_cam = 0;
  nb_nogav = 0;
  nb_etranger = 0;
  nb_majF = 0;
  nb_majM = 0;
  nb_minF = 0;
  nb_minM = 0;
  nb_masc = 0;
  nb_fem = 0;
 }
 this.title = 'stats_infraction';
 this.exportPdf(subgroupeds, 'infraction_arr', this.ttile_stat);
 return subgroupeds;
}

contructObjectInfractionByDept(datas) {
  this.title = 'stats_infraction_dept';
  let subgroupedSexe = null;
  let subgroupedGav = null;
  let subgroupedMaj = null;
  let subgroupedNat = null;
  let subgroupedSituation = null;
  let subgroupeds = [];
  let nb_p48gav = 0
  let nb_m48gav = 0;
  let nb_nogav = 0;
  let nb_libere = 0;
  let nb_defere = 0;
  let nb_fuite = 0;
  let nb_cam = 0;
  let nb_etranger = 0;
  let nb_majF = 0;
  let nb_majM = 0;
  let nb_minF = 0;
  let x_majM = 0;
  let nb_minM = 0;
  let nb_masc = 0;
  let x_majF = 0;
  let nb_fem = 0;
 const grouped = this.groupByList(datas, i => i.departement);
 for (let [key, value] of grouped) {
  subgroupedSexe = this.groupBy(value, i => i.sexe);
  subgroupedGav = this.groupBy(value, i => i.gav);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedNat = this.groupBy(value, i => i.camer);

  for (let [subkey, subvalue] of subgroupedSexe) {
    subgroupedMaj = this.groupBy(value, i => i.majeur);
    for (let [subkey0, subvalue0] of subgroupedMaj) {      
    if(subkey0 == 0 && subkey == 'M') nb_minM =  subvalue0.length;
    if(subkey0 == 0 && subkey == 'F') nb_minF =  subvalue0.length;
    if(subkey0 == 1 && subkey == 'M') nb_majM =  subvalue0.length;
    if(subkey0 == 1 && subkey == 'F') nb_majF =  subvalue0.length;
    if(subkey0 == 3 && subkey == 'M') x_majM =  subvalue0.length;
    if(subkey0 == 3 && subkey == 'F') x_majF =  subvalue0.length;
    }
  }
  for (let [subkey, subvalue] of subgroupedGav) {
    if(subkey == '1') nb_m48gav = subvalue.length;
    if(subkey == '2') nb_p48gav = subvalue.length;
    if(subkey == '3') nb_nogav = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedNat) {
    if(subkey == true) nb_cam = subvalue.length;
    if(subkey == false) nb_etranger = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedSituation) {
    if(subkey == '1defere') nb_libere = subvalue.length;
    if(subkey == 'nondefere') nb_defere = subvalue.length;
    if(subkey == 'enfuite') nb_fuite = subvalue.length;
  }

  subgroupeds.push(
    {
      'dept': key,
      'fait': 0,
      'mgav': nb_m48gav,
      'pgav':nb_p48gav,
      'gav': nb_nogav,
      'libere': nb_libere,
      'defere':nb_defere,
      'enfuite': nb_fuite,
      'camer': nb_cam,
      'etr': nb_etranger,
      'fMin': nb_minF,
      'mMiM': nb_minM,
      'majF': nb_majF,
      'majM': nb_majM,
      'xmajF': x_majF,
      'xmajM': x_majM
      
    }
  );
  nb_p48gav = 0
  nb_m48gav = 0;
  nb_libere = 0;
  nb_defere = 0;
  nb_fuite = 0;
  nb_cam = 0;
  x_majF = 0;
  x_majM = 0;
  nb_nogav = 0;
  nb_etranger = 0;
  nb_majF = 0;
  nb_majM = 0;
  nb_minF = 0;
  nb_minM = 0;
  nb_masc = 0;
  nb_fem = 0;
 }
 this.title = 'stats_infraction';
 this.exportPdf(subgroupeds, 'infraction_dept', this.ttile_stat);
 return subgroupeds;
}

contructObjectDatasByUnit(datas) {
  this.title = 'stats_enquete';
  this.ttile_stat = this.ttile_stat + 'PAR UNITE'

  let subgrouped = null;
  let subgroupeds = [];
  let nb_initiative_service = 0
  let nbplainte = 0;
  let nb_comission_derogatoire = 0;
  let nb_denonciation = 0;
  let nb_intruction_parquet = 0
 const grouped = this.groupBy(datas, i => i.unite);
 for (let [key, value] of grouped) {
    subgrouped = this.groupBy(value, i => i.typeSaisie);
    for (let [subkey, subvalue] of subgrouped) {
      if(subkey == 'initiative_service') nb_initiative_service = subvalue.length;
      if(subkey == 'plainte') nbplainte = subvalue.length;
      if(subkey == 'intruction_parquet') nb_intruction_parquet = subvalue.length;
      if(subkey == 'denonciation') nb_denonciation = subvalue.length;
      if(subkey == 'comission_derogatoire') nb_comission_derogatoire = subvalue.length;
    }
    subgroupeds.push(
      {
        'unit': key,
        'plainte': nbplainte,
        'initiative_service':nb_initiative_service,
        'intruction_parquet':nb_intruction_parquet,
        'denonciation': nb_denonciation,
        'comission_derogatoire':nb_comission_derogatoire,
        'total': nbplainte + nb_initiative_service + nb_intruction_parquet + nb_denonciation +nb_comission_derogatoire
      }
    );
    nb_initiative_service = 0
    nbplainte = 0;
    nb_comission_derogatoire = 0;
    nb_denonciation = 0;
    nb_intruction_parquet = 0
 }
 this.title = 'stats_enquete_unit';
 this.exportPdf(subgroupeds, 'unite', this.ttile_stat);
 return subgroupeds;
}

contructObjectDatasByDel(datas) {
  this.title = 'stats_enquete';
  this.ttile_stat = this.ttile_stat + 'PAR DELEGATION REGIONALE'

  let subgrouped = null;
  let subgroupeds = [];
  let nb_initiative_service = 0
  let nbplainte = 0;
  let nb_comission_derogatoire = 0;
  let nb_denonciation = 0;
  let nb_intruction_parquet = 0
 const grouped = this.groupBy(datas, i => i.delegation);
 for (let [key, value] of grouped) {
    subgrouped = this.groupBy(value, i => i.typeSaisie);
    for (let [subkey, subvalue] of subgrouped) {
      if(subkey == 'initiative_service') nb_initiative_service = subvalue.length;
      if(subkey == 'plainte') nbplainte = subvalue.length;
      if(subkey == 'intruction_parquet') nb_intruction_parquet = subvalue.length;
      if(subkey == 'denonciation') nb_denonciation = subvalue.length;
      if(subkey == 'comission_derogatoire') nb_comission_derogatoire = subvalue.length;
    }
    subgroupeds.push(
      {
        'del': key,
        'plainte': nbplainte,
        'initiative_service':nb_initiative_service,
        'intruction_parquet':nb_intruction_parquet,
        'denonciation': nb_denonciation,
        'comission_derogatoire':nb_comission_derogatoire,
        'total': nbplainte + nb_initiative_service + nb_intruction_parquet + nb_denonciation +nb_comission_derogatoire
      }
    );
    nb_initiative_service = 0
    nbplainte = 0;
    nb_comission_derogatoire = 0;
    nb_denonciation = 0;
    nb_intruction_parquet = 0
 }
 this.title = 'stats_enquete_del';
 this.exportPdf(subgroupeds, 'delegation', this.ttile_stat);
 return subgroupeds;
}

contructObjectInfractionByCat(datas) {
  this.title = 'stats_infraction';
  let subgroupedSexe = null;
  let subgroupedGav = null;
  let subgroupedMaj = null;
  let subgroupedNat = null;
  let subgroupedSituation = null;
  let subgroupeds = [];
  let nb_p48gav = 0
  let nb_m48gav = 0;
  let nb_nogav = 0;
  let nb_libere = 0;
  let nb_defere = 0;
  let nb_fuite = 0;
  let nb_cam = 0;
  let nb_etranger = 0;
  let nb_majF = 0;
  let x_majF = 0;
  let x_majM = 0;
  let nb_majM = 0;
  let nb_minF = 0;
  let nb_minM = 0;
  let nb_masc = 0;
  let nb_fem = 0;
 const grouped = this.groupByList(datas, i => i.infraction);
 console.log(grouped);
 for (let [key, value] of grouped) {
  subgroupedSexe = this.groupBy(value, i => i.sexe);
  subgroupedGav = this.groupBy(value, i => i.gav);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedSituation = this.groupBy(value, i => i.situation);
  subgroupedNat = this.groupBy(value, i => i.camer);

  for (let [subkey, subvalue] of subgroupedSexe) {
    subgroupedMaj = this.groupBy(value, i => i.majeur);
    for (let [subkey0, subvalue0] of subgroupedMaj) {
      if(subkey0 == 0 && subkey == 'M') nb_minM =  subvalue0.length;
      if(subkey0 == 0 && subkey == 'F') nb_minF =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'M') nb_majM =  subvalue0.length;
      if(subkey0 == 1 && subkey == 'F') nb_majF =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'M') x_majM =  subvalue0.length;
      if(subkey0 == 3 && subkey == 'F') x_majF =  subvalue0.length;
    }
  }
  for (let [subkey, subvalue] of subgroupedGav) {
    if(subkey == '1') nb_m48gav = subvalue.length;
    if(subkey == '2') nb_p48gav = subvalue.length;
    if(subkey == '3') nb_nogav = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedNat) {
    if(subkey == true) nb_cam = subvalue.length;
    if(subkey == false) nb_etranger = subvalue.length;
  }
  for (let [subkey, subvalue] of subgroupedSituation) {
    if(subkey == '1defere') nb_libere = subvalue.length;
    if(subkey == 'nondefere') nb_defere = subvalue.length;
    if(subkey == 'enfuite') nb_fuite = subvalue.length;
  }

  subgroupeds.push(
    {
      'infraction': key,
      'fait': 0,
      'mgav': nb_m48gav,
      'pgav':nb_p48gav,
      'gav': nb_nogav,
      'libere': nb_libere,
      'defere':nb_defere,
      'enfuite': nb_fuite,
      'camer': nb_cam,
      'etr': nb_etranger,
      'fMin': nb_minF,
      'mMiM': nb_minM,
      'majF': nb_majF,
      'majM': nb_majM,
      'xmajF': x_majF,
      'xmajM': x_majM

    }
  );
  nb_p48gav = 0
  nb_m48gav = 0;
  nb_libere = 0;
  nb_defere = 0;
  nb_fuite = 0;
  nb_cam = 0;
  nb_nogav = 0;
  nb_etranger = 0;
  nb_majF = 0;
  nb_majM = 0;
  x_majF = 0;
  x_majM = 0;
  nb_minF = 0;
  nb_minM = 0;
  nb_masc = 0;
  nb_fem = 0;
 }
 this.title = 'stats_infraction';
 this.exportPdf(subgroupeds, 'infraction', this.ttile_stat);
 return subgroupeds;
}

 exportPdf(datas, localite, ttile_stat) {
  this.dataForExcel = [];
  let excelDatas = datas;
  excelDatas = datas;//this.exportExcsk.setDataViewPdf(datas, this.title);
  excelDatas.forEach((row: any) =>{
    this.dataForExcel.push(Object.values(row))

  })
  let header = Object.keys(excelDatas[0]);
 // console.log(header);
  header = this.exportExcsk.getHeaderValue(this.title, 'pdf');
 // console.log(this.dataForExcel);
 // console.log(header);
  this.pdfService.generatePdf(header, this.dataForExcel, this.title, ttile_stat);
}

getNextDay(selectedDate: Date) {
  let d = new Date(selectedDate);
  d.setDate(d.getDate() + 1);
  let date = this.datePipe.transform(d, 'yyyy-MM-dd');
  return new Date(date);
}

getTomorrow(){
  const today = new Date()
  const tomorrow = new Date(today)
  tomorrow.setDate(tomorrow.getDate() + 1)
  return tomorrow;

}


}

