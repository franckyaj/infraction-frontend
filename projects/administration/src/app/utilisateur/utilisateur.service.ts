import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToolService } from 'projects/tool/src/public-api';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  constructor(
    private toolService: ToolService,
    private http: HttpClient
  ) { }

  addUser(obj, link) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.adminPath + link, obj)
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  delete(obj) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.adminPath + '/user/delete/'+obj.id, {})
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  listUsers() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.get(this.toolService.address + '/' + this.toolService.adminPath + '/user/listAll')
    .pipe(map((res: any) => {
      return res;
      } ));
  }
}
