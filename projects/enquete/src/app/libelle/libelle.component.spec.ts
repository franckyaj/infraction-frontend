import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibelleComponent } from './libelle.component';

describe('LibelleComponent', () => {
  let component: LibelleComponent;
  let fixture: ComponentFixture<LibelleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibelleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibelleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
