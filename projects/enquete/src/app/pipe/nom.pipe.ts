import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nom'
})
export class NomPipe implements PipeTransform {

  transform(value: any[], ...args: unknown[]): unknown {
    let res = '';
    for(let i of value) {
      res = res + i.nom + ' ' + i.prenom + '; ';
    }
    return res;
  }

}
