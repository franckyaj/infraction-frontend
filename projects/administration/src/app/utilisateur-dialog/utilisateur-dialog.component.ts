import { QueryBindingType } from '@angular/compiler/src/core';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToolService } from 'projects/tool/src/public-api';
import { Utilisateur } from '../model/utilisateur';
import { UtilisateurService } from '../utilisateur/utilisateur.service';

@Component({
  selector: 'app-utilisateur-dialog',
  templateUrl: './utilisateur-dialog.component.html',
  styleUrls: ['./utilisateur-dialog.component.css']
})
export class UtilisateurDialogComponent implements OnInit {

  user: Utilisateur;
  users = [];
  profiles = [];
  roles = [];
  path: any;
  isInit = false;
  currentUSerLogin: any;
  maxDate: any;
  minDate: any;
  showActive = false;
  role: any;

  constructor(
    public dialogRef: MatDialogRef<UtilisateurDialogComponent>,
    private service: UtilisateurService,
    private toolService: ToolService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    if(this.data != null) this.user = this.data.user;
  }

  loadAll() {
    this.user = new Utilisateur();
    let currentYear = new Date().getFullYear();
    let currentMonth = new Date().getMonth();
    let currentday = new Date().getDate();

    this.minDate = new Date(currentYear-70, currentMonth,1);
    this.maxDate = new Date(currentYear-20, currentMonth, currentday);
    if(this.data != null) {
     this.user = this.data.user;
     this.profiles = this.data.profile;
     this.role = this.data.roles[0];
     this.isInit = this.data.isInit;
     this.path = this.data.path;
    // alert(this.isInit);
    }

 }

 ngOnInit() {
  this.loadAll();
  this.currentUSerLogin = JSON.parse(localStorage.getItem('user')).firstName + ' ' +
  JSON.parse(localStorage.getItem('user')).lastName ; // get connected user login
}

add() {
  this.roles = [];
  this.roles.push(this.role);
  this.user.roles = this.getRoleObj(this.roles);
  this.toolService.isLoading.next(true);
  if(this.user.id == null) this.user.userCreated = this.currentUSerLogin;
  if(this.user.id != null) this.user.userModified = this.currentUSerLogin;
  if(this.isInit) this.user.defaultPassword = 'ChangeMe';
  console.info(this.user)
  this.service.addUser(this.user, this.path)
  .subscribe(
    (res) => this.onUserAddSuccess(res),
    (error) => this.onError(error)
  )
}

getRoleObj(roles) {
  let r = [];
  console.log(this.user.roles);
  for(let x of roles) {
    for(let i of this.profiles) {
      if(x == i.id) r.push(i);
    }
  }
  return r;
}

cancel() {
  this.dialogRef.close(false);
}

onError(error){
  this.toolService.isLoading.next(false);
  this.isInit = false;
}

onUserAddSuccess(res) {
  this.toolService.isLoading.next(false);
   this.isInit = false;
   this.dialogRef.close(true);
 }

}
