import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'projects/tool/src/lib/auth/auth.guard';
import { AdministrationSharedModule } from '../../../administration/src/app/app.module';
import { EnqueteSharedModule } from '../../../enquete/src/app/app.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'main',
    component: AppComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        component: HomeComponent
      }
    ]
  },
  {
    path: 'enquete', 
    loadChildren: ()=> import('../../../enquete/src/app/app.module').then(m=>m.EnqueteSharedModule),
   // loadChildren: '../../../../projects/accueil/src/app/app.module#AccueilSharedModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'administration',
    loadChildren: ()=> import('../../../administration/src/app/app.module').then(m => m.AdministrationSharedModule),
   // loadChildren: '../../../../projects/administrateur/src/app/app.module#AdministrationSharedModule',
    canActivate: [AuthGuard],

  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes),
    AdministrationSharedModule.forRoot(),
    EnqueteSharedModule.forRoot()

  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
