import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { ExcelService } from 'projects/tool/src/lib/service/excel.service';
import { PdfService } from 'projects/tool/src/lib/service/pdf.service';
import { ToolService } from 'projects/tool/src/public-api';
import { of } from 'rxjs';
import { distinct } from 'rxjs/operators';
import { ExcelDataType } from 'xlsx';
import { ChartComponent } from '../chart/chart.component';
import { EnquetedialogComponent } from '../enquetedialog/enquetedialog.component';
import { HomeComponent } from '../home/home.component';
import { InfractiondialogComponent } from '../infractiondialog/infractiondialog.component';
import { MiseencauseComponent } from '../miseencause/miseencause.component';
import { SearchDto } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import { StupefiantComponent } from '../stupefiant/stupefiant.component';
import { VehiculevoleComponent } from '../vehiculevole/vehiculevole.component';
import { VictimeComponent } from '../victime/victime.component';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.css'],
  providers: [MessageService]
})
export class ReportingComponent implements OnInit, OnDestroy {

  dateFin: any;
  datedebut: any;
  reportType: String;
  datas: any[];
  title: any;
  cols = [];
  data: any;
  dataForExcel = [];
  seachDto: SearchDto;
  unites = [];
  

  gav = [
    { code: '1', label: 'Moins de 48h' },
    { code: '2', label: 'Plus de 48h' },
  ];
  titles = [
    { code: 'victime', label: 'VICTIMES', path: '',
    cols: [
      { field: 'nom', header: 'Nom' },
      { field: 'prenom', header: 'Prénom' },
      { field: 'sexe', header: 'Sexe' },
      { field: 'nationalite', header: 'Nationalité' },
      { field: 'adresse', header: 'Adresse' },
      { field: 'etat', header: 'Etat' }
    ]
  },
    { code: 'miseencause', label: 'MISE EN CAUSE' , path: '' ,
    cols: [
      { field: 'nom', header: 'Nom' },
      { field: 'prenom', header: 'Prénom' },
      { field: 'sexe', header: 'Sexe' },
      { field: 'nationalite', header: 'Nationalité' },
      { field: 'implication', header: 'Implication' },
      { field: 'situation', header: 'Situation' },
      { field: 'etat', header: 'Etat' }
    ] },
    { code: 'search-miseencause', label: 'MISE EN CAUSE' , path: '' ,
    cols: [
      { field: 'nom', header: 'Nom' },
      { field: 'prenom', header: 'Prénom' },
      { field: 'sexe', header: 'Sexe' },
      { field: 'nationalite', header: 'Nationalité' },
      { field: 'implication', header: 'Implication' },
      { field: 'situation', header: 'Situation' },
      { field: 'etat', header: 'Etat' }
    ] },
    { code: 'vehiculevole', label: 'VEHICULES VOLES' , path: '' ,
    cols: [
      { field: 'marque', header: 'Marque' },
      { field: 'immatriculation', header: 'Immatriculation' },
      { field: 'etat', header: 'Etat' },
      { field: 'chasis', header: 'Chasis' },
      { field: 'proprietaire', header: 'Propriétaire' }
    ] },
    { code: 'enquete', label: 'ENQUETE', path: '' ,
    cols: [
      { field: 'referenceProcedure', header: 'Reference Procédure' },
      { field: 'affaire', header: 'Affaire' },
      { field: 'victime', header: 'Victime' },
      { field: 'miseencause', header: 'Mise en cause' },
      { field: 'resumeFait', header: 'Resume des faits' }
    ]  },
    { code: 'infraction', label: 'INFRACTION', path: '' ,
    cols: [
      { field: 'categorie', header: 'Categorie de l infraction' },
      { field: 'infractionCommise', header: 'Infraction comise' },
      { field: 'date', header: 'Date' },
      { field: 'typeSaisie', header: 'Type de saisie' },
      { field: 'rreferenceSaisie', header: 'Reference de saisie' },
      { field: 'adressePrecise', header: 'Adresse de saisie' },
      { field: 'localite', header: 'Localité' },


    ]  },
    { code: 'stupefiant', label: 'STUPEFIANT', path: '' ,
    cols: [
      { field: 'nature', header: 'Nature' },
      { field: 'quantite', header: 'Quantité' },
      { field: 'provenance', header: 'Provenance' },
      { field: 'destinattion', header: 'Destination' },
      { field: 'etat', header: 'Etat' }
    ]  },
  ];
  selectedItem: any;
  delegations = [];
  delegation: any;

  today = new Date();
  month = this.today.getMonth();
  year =   this.today.getFullYear();
  cuti: any;
  uniteDatas = [];
  roles = ['ADMIN_EDIT_USER', 'EDIT_ALL_DATA'];

  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private pdfService: PdfService,
    private datePipe: DatePipe,
    private exportExcsk: ExcelService,
    private dialog: MatDialog,
    private permission: NgxPermissionsService,
    private tool: ToolService,
    private service: EnqueteService
  ) { }

  loadAll(){
    this.datas = [];

    this.permission.hasPermission(this.roles)
    .then(
      res => {
        if(res == true) {}
        else {
          this.cuti = JSON.parse(localStorage.getItem('user')).login;
        }
      }
    )
    

    this.route.params.subscribe( params => this.initDatas(params.typereport) );

  }
  ngOnInit(): void {
    this.loadAll();
  }

  loadUnites() {
    let path = '/localite/unite'
    this.service.listDatas(path)
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  onLoadSuccess(res) {
    this.uniteDatas = res.unites;
    let tmp = [];
    for(let r of res.unites) {
      tmp.push(r.delegation);
    }
    
    this.delegations = tmp.filter((value, index, categoryArray) => categoryArray.indexOf(value) === index);
  }

  getUnite(event) {
    this.unites = [];
    console.log(event.value);
    let tmp = this.uniteDatas.filter(x => x.delegation == event.value);
    for(let r of tmp) {
      this.unites.push(r.unite);
    }
  }

  initDatas(typeReport) {
    this.seachDto = new SearchDto();
    this.seachDto.debut = new Date(this.year, this.month,1);
    this.seachDto.fin = new Date(this.year, this.month,this.today.getDate());
    this.tool.inReportObs.next(true);
    this.datas = [];
    let obj = this.titles.filter(item => item.code == typeReport);
    this.title = obj[0];
    if(this.title.code == 'enquete') this.loadUnites();

  }


  loadDatas() {
    this.spinner.show();
    let obj = {};
    this.seachDto.cuti = this.cuti;
    if(this.seachDto.fin != null) this.seachDto.fin =  this.getTomorrow();// new Date(this.year, this.month,this.today.getDate());
    this.datas = [];
    if(this.seachDto.referenceProcedureLike == '') this.seachDto.referenceProcedureLike = null;
   
    let paht = '/'+this.title.code + '/filter'
    if(this.title.code == 'miseencause' || this.title.code == 'search-miseencause') {
      paht = '/enquete/filter-mec'
      this.seachDto.nomLike = this.seachDto.nomLike.toUpperCase();
    }

    if( this.title.code == 'search-miseencause') {
      this.seachDto.fin = null;
      this.seachDto.debut = null;
    }
    
    console.log(paht)
    console.log(this.seachDto);
    this.service.list(this.seachDto, paht)
    .subscribe(
      (res) => this.onFilterSuccess(res),
      (error) => this.onError(error)
    )
  }

  onFilterSuccess(res) {
    this.spinner.hide();
    console.log(res);
    this.datas = res;
     this.seachDto.fin = new Date(this.year, this.month,this.today.getDate());
     
  }

  onError(error) {
    this.spinner.hide();
    this.seachDto.fin = new Date(this.year, this.month,this.today.getDate());
    this.messageService.add({severity:'error', summary: 'Attention',life:4600, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  //excel
  extractDatas() {
    this.dataForExcel = [];
    let excelDatas = this.datas;
    excelDatas = this.exportExcsk.setDataView(this.datas, this.title.code);
    excelDatas.forEach((row: any) =>{
      this.dataForExcel.push(Object.values(row))
    })
    let header = Object.keys(excelDatas[0]);
    header = this.exportExcsk.getHeaderValue(this.title.code);
    let date = this.datePipe.transform(new Date(), 'dd/MM/yyyy')
    this.exportExcsk.generateExcelFile(this.dataForExcel, this.title.label, header, date)
  }

  

  onRowSelect(event) {
    let data = event.data;
    let component = null;
    let width = '';
    console.log(event);
    if(this.title.code == 'infraction'){
      component = InfractiondialogComponent;
      width = '60%';
    } 
    if(this.title.code == 'victime') {
      component = VictimeComponent;
      width = '30%';
    }
    if(this.title.code == 'stupefiant') {
      component = StupefiantComponent;
      width = '30%';
    }
    if(this.title.code == 'miseencause' || this.title.code == 'search-miseencause') {
      component = MiseencauseComponent;
      width = '65%';
    }
    if(this.title.code == 'vehiculevole') {
      component = VehiculevoleComponent;
      width = '30%';
    }
    if(this.title.code == 'enquete') {
      component = EnquetedialogComponent;
      width = '30%';
    }

    let dialogRef = this.dialog.open(component, {
      disableClose: false,
      width: width,
      data: {obj:null, data: event.data, type:'edit'}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null) this.loadDatas();
      }
    )
  }


  //pdf
  exportPdf() {
    this.dataForExcel = [];
    let excelDatas = this.datas;
    excelDatas = this.exportExcsk.setDataViewPdf(this.datas, this.title.code);
    console.info(excelDatas);
    excelDatas.forEach((row: any) =>{
      this.dataForExcel.push(Object.values(row))

    })
    let header = Object.keys(excelDatas[0]);
    header = this.exportExcsk.getHeaderValue(this.title.code, 'pdf');
    console.info(header);

    this.pdfService.generatePdf(header, this.dataForExcel, this.title.label,  'COMPTE RENDU '+this.title.label );
  }

  showChart() {
    let res = this.setDatasChart();
    this.dialog.open(ChartComponent, {
      width: '70%',
      disableClose: false,
      data: res
    });
  }

  setDatasChart() {
    let counts = {}
    let result = [0,0,0,0,0,0,0,0,0,0,0,0];
    let year = new Date().getFullYear();
    let months = [];
    for(let i of this.datas) {
      for(let x of i.infractionCommises) {
        months.push(new Date(i.date).getMonth())
      }
    }

    for(let i=0; i<months.length; i++) {
      if (counts[months[i]]){
        counts[months[i]] += 1
        } else {
        counts[months[i]] = 1
        }
    }
    for (let prop in counts){
      if (counts[prop] >= 2){
        let x = 
          console.log(prop + " counted: " + counts[prop] + " times.");
          result[prop] = counts[prop];
      }
  }
  return result;
  }

  ngOnDestroy() {
    this.tool.inReportObs.next(false);
  }

  getTomorrow(){
    const today = new Date()
    const tomorrow = new Date(today)
    tomorrow.setDate(tomorrow.getDate() + 1)
    return tomorrow;

  }

  
}
