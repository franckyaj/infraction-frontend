import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthGuard } from 'projects/tool/src/lib/auth/auth.guard';
import { AppComponent } from './app.component';
import { ExportComponent } from './export/export.component';
import { ParaminfractionComponent } from './paraminfraction/paraminfraction.component';
import { ProfilComponent } from './profil/profil.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';

const routes: Routes = [
  
  /*{
    path: 'administration/home',
    component: DashboardComponent,

  },
  {
    path: 'administration/profil',
    component: GestionProfilComponent,

  },
  {
    path: 'administration/user',
    component: GestionUtilisateurComponent,

  }*/
  {
    path: 'administration',
    component: AppComponent,
    canActivate: [AuthGuard],
    children: [
      
      {
        path: 'home',
        component: ProfilComponent,
        //canActivate: [NgxPermissionsGuard],
        /*data: {
          permissions: {
            only: ['ADMIN_ADD_PROFIL'],
            redirectTo: '/main/home'
          }        
        },*/
      },
      {
        path: 'export/:type',
        component: ExportComponent,
      },
      {
        path: 'param-infraction',
        component: ParaminfractionComponent,
      },
      
     {
        path: 'user',
        component: UtilisateurComponent,
        /*canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ADMIN_EDIT_USER'],
            redirectTo: '/main/home'
          }        
        },*/
      },
      /*{
        path: 'examen',
        component: GestionExamenComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ADMIN_EDIT_EXAM'],
            redirectTo: '/main/home'
          }        
        },
      },
      {
        path: 'patient',
        component: PatientComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ADMIN_EDIT_USER'],
            redirectTo: '/main/home'
          }        
        },
      },

      {
        path: 'user',
        component: GestionUtilisateurComponent,

      }*/
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
