import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import * as XLSX from 'xlsx';
import { Locality, Unite } from '../model/locality';
import { EnqueteService } from '../service/enquete.service';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css'],
  providers: [MessageService]
})
export class ExportComponent implements OnInit {

  localite: Locality;
  unite:Unite;
  localites: Locality[];
  unites: Unite[];
  type = '';
  

  constructor(
    private service: EnqueteService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.localite = new Locality();
    this.unite = new Unite();
    this.unites = [];
    this.localites = [];
  }

  onFileChange(ev, type) {
    this.type = type;
    if(type == 'unite') {
      this.onFileChange0(ev);
    }
    else {  
      this.onFileChange1(ev)
    }
  }

  onFileChange1(ev) {
    this.type = 'localite';
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
     // document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
      this.saveData(JSON.parse(dataString).Sheet1);
    }
    reader.readAsBinaryString(file);

  }

  saveData(datas) {
    for(let i of datas) {
      this.localite = new Locality();
      let keys = Object.keys(i);
      let entries = Object.entries(i);
      for(const [prop,  val] of entries ) {
        if(prop == 'REGION') this.localite.region = val;
        if(prop == 'DEPARTEMENT') this.localite.departement = val;
        if(prop == 'ARRONDISSEMENT') this.localite.arrondissement = val;
      }
      this.localites.push(this.localite);
    }

  }

  saveDatas() {
    let obj = null
    let path = null;
    if(this.type == 'localite') {
      obj = {
        localites: this.localites
      };
      path = '/localite/save';
    }
    else {
      obj = {
        unites: this.unites
      };
      path = '/localite/unite/save';

    }
    
    this.service.saveLocalite(obj,path)
    .subscribe(
      (res) => this.onSuccess(res),
      (error) => this.onError(error)
    )
  }

  onSuccess(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:296000, detail: 'Sauvegarde effectuée avec succès!!'});

  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  onFileChange0(ev) {
    this.type = 'unite';
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
      this.saveData0(JSON.parse(dataString).Sheet1);
    }
    reader.readAsBinaryString(file);

  }

  saveData0(datas) {
    console.log(datas);
    for(let i of datas) {
      this.unite = new Unite();
      let keys = Object.keys(i);
      let entries = Object.entries(i);
      for(const [prop,  val] of entries ) {
        if(prop == 'Direction de la Police Judiciaire') this.unite.delegation = val;
        if(prop == 'DPJ') this.unite.unite = val;
      }
      this.unites.push(this.unite);
    }

  }

  getRegion(value) {
    let res = null;
    let regions = ['centre', 'adamaoua','nord', 'sud','sud-ouest', 'sud-est', 'nord-ouest','est', 'ouest','littoral',  'extrême-nord']
    for(let i of regions) {
      if(value.toUpperCase().indexOf(i.toUpperCase()) > -1) res = i;
    }
    return res;
  }

}
