import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { MenuItem } from 'primeng/api';
import { ToolService } from 'projects/tool/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'module';
  username = '';

  items: MenuItem[];
  perm = [];

  constructor(
    private router: Router,
    private toolService: ToolService,
    //private userIdle: UserIdleService,
    private permission: NgxPermissionsService
  ) {}

  ngOnInit() {
    this.loadAll();
    this.items = [
//      {label: 'Quitter', icon: 'pi pi-times', routerLink: ['/main/home']},
      {separator: true},
      {label: 'Se deconnecter', icon: 'pi pi-sign-out', command: () =>{
        this.logout()
      }},
  ];
  }

  loadAll() {
    this.perm = this.toolService.userPermission;
    if(this.perm.length == 0) this.perm = JSON.parse(localStorage.getItem('role'));
  

    this.toolService.userPermission = this.perm;
    localStorage.setItem('permission', JSON.stringify(this.perm));
    for(let i of this.perm) {
      this.permission.addPermission(i);
    }
    this.getUserName();
    localStorage.setItem('role', JSON.stringify(this.perm));
  }

  getUserName() {
    let user = JSON.parse(localStorage.getItem('user'));
    if(user.lastName == null) {
      this.username = user.firstName.toUpperCase();
    }
    else {
      this.username = user.firstName.toUpperCase() + ' ' + user.lastName;
    }  
  }

  logout() {
    this.permission.flushPermissions();
    localStorage.clear();
    this.router.navigate([`/login`]);
    location.reload();

  }
}
