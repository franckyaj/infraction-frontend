import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ToolComponent } from './tool.component';

import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { MAT_DATE_LOCALE, , , MatSelectModule, ,  } from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokeninterceptorService } from './tokeninterceptor.service';
import { NgxLoadingModule } from 'ngx-loading';
import {SplitButtonModule} from 'primeng/splitbutton';
import { NgxPermissionsModule } from 'ngx-permissions';
import {ToastModule} from 'primeng/toast';
import {TableModule} from 'primeng/table';
import { MessageService } from 'primeng/api';
import { DateformatPipe } from './pipe/dateformat.pipe';
import { FormatrolePipe } from './pipe/formatrole.pipe';
import { ToolService } from './tool.service';
import { ShareService } from './service/share.service';
import { RolenamePipe } from './pipe/rolename.pipe';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { CdkStepper, CdkStepperModule } from '@angular/cdk/stepper';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { NgxSpinnerModule } from "ngx-spinner";
import { DatePipe } from '@angular/common';
import { TokeninterceptorsService } from './interceptor/tokeninterceptor.service';

export function initalizerApp(appitializeService: ToolService) {
  return (): Promise<any> =>{
    return appitializeService.getToken();
  }
}

const MaterialComponent = [
  MatInputModule,
  MatDialogModule,
  MatFormFieldModule,
  MatSelectModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatCheckboxModule,
  NgxSpinnerModule,
  MatStepperModule,
  MatAutocompleteModule,
  CdkStepperModule

] 

@NgModule({
  declarations: [ToolComponent, DateformatPipe, FormatrolePipe, RolenamePipe],
  imports: [
    FormsModule,
    HttpClientModule,
    NgxLoadingModule,
    SplitButtonModule,
    NgxPermissionsModule,
    NgxSpinnerModule,
    ToastModule,
    TableModule,
    MaterialComponent,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    ToolComponent,
    FormsModule,
    HttpClientModule,
    NgxLoadingModule,
    SplitButtonModule,
    NgxPermissionsModule,
    DateformatPipe,FormatrolePipe, RolenamePipe,
    ToastModule,
    MatNativeDateModule,
    TableModule,
    MaterialComponent,
    ReactiveFormsModule,
  ],
  providers: [
    MessageService,
    DatePipe,
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    {
      provide: APP_INITIALIZER,
      useFactory: initalizerApp,
      deps: [ ToolService ],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokeninterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokeninterceptorsService,
      multi: true
    }
  ]
})
export class ToolModule { }
