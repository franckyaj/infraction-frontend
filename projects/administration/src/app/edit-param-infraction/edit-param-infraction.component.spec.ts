import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditParamInfractionComponent } from './edit-param-infraction.component';

describe('EditParamInfractionComponent', () => {
  let component: EditParamInfractionComponent;
  let fixture: ComponentFixture<EditParamInfractionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditParamInfractionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditParamInfractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
