export interface AppConfig {
    host: string; //host backend
    protocole: String; //protocole backend
    port: String; //port backend
    client_id: any;
    client_secret: any;
    username: any;
    password: any;
    grant_type: any;
}
