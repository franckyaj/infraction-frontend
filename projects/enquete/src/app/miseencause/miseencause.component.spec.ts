import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiseencauseComponent } from './miseencause.component';

describe('MiseencauseComponent', () => {
  let component: MiseencauseComponent;
  let fixture: ComponentFixture<MiseencauseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiseencauseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiseencauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
