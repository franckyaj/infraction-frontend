export class Utilisateur {
    id: number;
    firstName: String;
    lastName: String;
    login: String;
    password: String;
    phoneNumber: String;
    email: String;
    dateOfBirth: Date;
    roles: any[];
    defaultPassword: String;
    active: boolean;
    userCreated: String;
    userModified: String;
}
