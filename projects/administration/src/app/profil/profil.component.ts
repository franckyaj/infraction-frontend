import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { ToolService } from 'projects/tool/src/public-api';
import { ProfilDialogComponent } from '../profil-dialog/profil-dialog.component';
import { ProfilService } from './profil.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
  providers: [MessageService]

})
export class ProfilComponent implements OnInit {

  profils = [];
  profil: any = {};
  habilitations = [];

  constructor(
    private toolService: ToolService,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    private service: ProfilService
  ) { }

  ngOnInit(): void {
    this.habilitations = this.toolService.applicationPermissions;
    this.loadAll();
  }

  loadAll() {
    this.loadProfile();
  }

  openAddUser(obj) {
    let d = this.dialog.open(ProfilDialogComponent, {
      width: '95%',
      disableClose: false,
      position: { top: '3%', left: '18%'},
      data: { habilitations: this.habilitations, profil:this.profil }
    })
    d.afterClosed().subscribe(result => {
      this.loadProfile();
      if(result == true){
        this.messageService.add({severity:'success', summary: 'Success', detail: 'Opération effectuée avec succès'});
      }
   });
  }

  loadProfile() {
    this.spinner.show();
    this.service.listProfil()
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  onError(error) {
    this.spinner.hide();
    console.log(error);
  }

  onLoadSuccess(res) {
    this.spinner.hide();
    this.profils = res;
    
  }

  open(obj) {
    this.profil = obj;
   // this.getProfilObj(this.profil.privileges);
   let d = this.dialog.open(ProfilDialogComponent, {
    width: '85%',
    position: { top: '3%', left: '18%'},
    disableClose: false,
    panelClass: 'custom-modalbox',
    data: { habilitations: this.habilitations, profil:this.profil }
  })
  d.afterClosed().subscribe(result => {
    this.loadProfile();
    if(result == true){
      this.messageService.add({severity:'success', summary: 'Success', detail: 'Opération effectuée avec succès'});
    }
 });
  }

}
