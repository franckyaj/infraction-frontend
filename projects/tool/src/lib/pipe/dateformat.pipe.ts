import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateformat'
})
export class DateformatPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    if(value != null) {
      let day = value.split('-')[2];
      let month = value.split('-')[1];
      let year = value.split('-')[0];
      return day + '/' + month + '/' + year;
    }
    else{
      return '';
    }
  }

}
