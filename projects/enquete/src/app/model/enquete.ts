import { ClassField } from "@angular/compiler";
import { Locality } from "./locality";

export class Enquete {
    referenceProcedure: String;
    parquetCompetant: String;
    affaire: String;
    infraction: Infraction;
    cadreJuridique: String;
    coordonneesOpj: any;
    coordonneesAssistant: any;
    resumeFaits: String;
    victimes: Victime[];
    miseencauses: MiseEnCause[];
    stupefiants: Stupefiant[];
    armes: Arme[];
    vehiculeVoles: VehiculeVole[];
    lieu: any;
    active: boolean;
    dateCreation: Date;
    cuti: any;
}

export class Stupefiant {
    id: any;
    nature: String;
    destinattion: String;
    provenance: String;
    etat: String;
    quantite: number;
    active: boolean;
    dateCreation: Date;
    cuti: any;

}

export class VehiculeVole {
    id: any;
    marque: String;
    immatriculation: String;
    chasis: String;
    proprietaire: String;
    etat: String;
    active: boolean;
    dateCreation: Date;
    cuti: any;

}

export class Arme {
    id: any;
    type: any;
    numero: String;
    etat: String;
    active: boolean;
    dateCreation: Date;
    cuti: any;
}

export class Victime {
    id: any;
    nom: String;
    prenom: String;
    sexe: String;
    dateNaissance: Date;
    lieuNaissance: String;
    nationalite: String;
    adresse: String;
    telephone: String;
    etat: String;
    active: boolean;
    dateCreation: Date;
    cameroonian: boolean;
    cuti: any;

}

export class MiseEnCause {
    id: any;
    nom: String;
    prenom: String;
    sexe: String;
    dateNaissance: Date;
    lieuNaissance: String;
    nationalite: String;
    adresse: String;
    telephone: String;
    situation: String;
    gav: String;
    implication: String;
    numeroCni: String;
    alias: String;
    etat: String;
    active: boolean;
    infractionCommises: any;
    dateCreation: Date;
    cameroonian: boolean;
    cuti: any;
    neevers: boolean;


}

export class Infraction {
    typeSaisie: String;
    rreferenceSaisie: String;
    infractionCommises: any;
    categorie: String;
    date: Date;
    adressePrecise: String;
    id: any;
    localite: Locality;
    active: boolean;
    cuti: String;
    
}

export class SearchDto {
    debut: Date;
    fin: Date;
    referenceProcedureLike: String;
    gav: String;
    unite: String;
    nomLike: any;
    cuti: any;
}

export class ReportMec {
    nom: String;
    prenom: String;
    infractions: any[];
    
}