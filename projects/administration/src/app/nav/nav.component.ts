import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor() { }

  myJs1() {
    if( $(".show1").css('display') == 'none'){
      $(".show1").slideDown()
    }
    else {
      $(".show1").slideUp()
    }   
}

  ngOnInit(): void {
  }

}
