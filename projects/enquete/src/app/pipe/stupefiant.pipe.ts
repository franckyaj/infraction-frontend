import { Pipe, PipeTransform } from '@angular/core';
import { Stupefiant } from '../model/enquete';

@Pipe({
  name: 'stupefiant'
})
export class StupefiantPipe implements PipeTransform {

  transform(value: Stupefiant[], ...args: unknown[]): unknown {
    let res = '';
    for(let i of value) res = res + i.nature + ';' 
    return res;  }

}
