import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquetedialogComponent } from './enquetedialog.component';

describe('EnquetedialogComponent', () => {
  let component: EnquetedialogComponent;
  let fixture: ComponentFixture<EnquetedialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnquetedialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquetedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
