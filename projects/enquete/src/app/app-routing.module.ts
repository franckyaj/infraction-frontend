import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AppComponent } from './app.component';
import { ExportComponent } from './export/export.component';
import { HomeComponent } from './home/home.component';
import { ParamInfraction } from './model/locality';
import { ParaminfractionComponent } from './paraminfraction/paraminfraction.component';
import { ReportingComponent } from './reporting/reporting.component';
import { StatistiqueComponent } from './statistique/statistique.component';

const routes: Routes = [
  
      
  {
    path: 'enquete',
    component: AppComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['EDIT_DATA', 'DELETE_DATA','SAISI_FORM'],
            redirectTo: '/main/home'
          }        
        }
      },
      {
        path: 'report',
        redirectTo: 'report/enquete',
      },
      {
        path: 'report/:typereport',
        component: ReportingComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['EDIT_DATA', 'DELETE_DATA','SAISI_FORM'],
            redirectTo: '/main/home'
          }        
        }
      },
      {
        path: 'statistique',
        component: StatistiqueComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['EDIT_DATA', 'DELETE_DATA','SAISI_FORM'],
            redirectTo: '/main/home'
          }        
        }
      },
      {
        path: 'export',
        component: ExportComponent,
      },
      
      {
        path: 'param-infraction',
        component: ParaminfractionComponent,
      }
      
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
