import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Arme } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-arme',
  templateUrl: './arme.component.html',
  styleUrls: ['./arme.component.css'],
  providers: [MessageService]
})
export class ArmeComponent implements OnInit {

  arme: Arme;
  armes: Arme[];
  cuti: any;

  etat = [
    { code: 'recherche', label: 'Recherché' },
    { code: 'saisi', label: 'Saisi' }
  ];
  filterTypes: Observable<any[]>;
  myControl = new FormControl();


  constructor(
    public dialogRef: MatDialogRef<ArmeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: EnqueteService,
    private messageService: MessageService
  ) { }

  loadAll() {
    this.arme = new Arme();
    this.cuti = JSON.parse(localStorage.getItem('user')).login;

    this.armes = [];
    this.filterTypes = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => name ? this._filter(name) : this.data.obj.slice())
    );
  }

  displayFn(name: any): string {
    return name ? name : '';
  }
  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.data.obj.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  
  ngOnInit(): void {
    this.loadAll();
  }


  save() {
    this.arme.cuti = this.cuti;
    this.armes.push(this.arme);
    this.dialogRef.close(this.armes);

   /* let obj = {
      armes: this.armes
    };
    this.service.add(obj, '/arme/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )*/
  }
  onSaveSuccess(res) {
    this.dialogRef.close(res)
  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  cancel() {
    this.arme = new Arme();
    this.armes = [];
    this.dialogRef.close();
  }

  delete() {
    this.arme.active = false;
    this.service.add(this.arme, '/arme/save')
    .subscribe(
      (res) => this.onDelete(res),
      (error) => this.onError(error)
    )
  }
  onDelete(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:46000, detail: 'Opération effectuée avec succès!!!'});
  }

  addNew() {
    this.arme.cuti = this.cuti;
    this.armes.push(this.arme);
    this.arme = new Arme();
    console.log(this.armes);
  }



}
