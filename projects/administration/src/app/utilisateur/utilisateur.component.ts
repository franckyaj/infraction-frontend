import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { ToolService } from 'projects/tool/src/public-api';
import { Utilisateur } from '../model/utilisateur';
import { ProfilService } from '../profil/profil.service';
import { UtilisateurDialogComponent } from '../utilisateur-dialog/utilisateur-dialog.component';
import { UtilisateurService } from './utilisateur.service';

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {

  user: Utilisateur;
  users = [];
  profiles = [];
  roles = [];
  path: any;
  isInit = false;
  

  constructor(
    private profilService: ProfilService,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    private toolService: ToolService,
    private service: UtilisateurService
  ) { }

  getRolesId(roles) {
    let r = [];
    for(let i of roles) {
      r.push(i.id);
    }
    return r;
  }

  getRoleObj(roles) {
    let r = [];
    for(let x of roles) {
      for(let i of this.profiles) {
        if(x == i.id) r.push(i);
      }
    }
    return r;
  }

  onLoadSuccess(res) {
    console.log(res);
    this.profiles = res;
  }

  onError(error){
    this.spinner.hide();
    this.toolService.isLoading.next(false);
    this.isInit = false;
  }

  openAddUser(obj) {
    let d = this.dialog.open(UtilisateurDialogComponent, {
      width: '95%',
      disableClose: false,
      position: { top: '3%', left: '18%'},
      data: {user: this.user, profile: this.profiles,isInit: this.isInit, path:this.path, roles: this.roles}
    })
    d.afterClosed().subscribe(result => {
      this.loadUsers();
      if(result == true) {
        this.messageService.add({severity:'success', summary: 'Success', detail: 'Opération effectuée avec succès'});
      }
   });
  }

  loadProfile() {
    this.profilService.listProfil()
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  onLoadUserSuccess(res) {
    this.spinner.hide();
    this.toolService.isLoading.next(false);
    this.users = res;
    this.isInit = false;
    this.user= new Utilisateur();
    this.roles = [];
  }

  loadUsers() {
    this.spinner.show();
    this.toolService.isLoading.next(true);
    this.service.listUsers()
    .subscribe(
      (res) => this.onLoadUserSuccess(res),
      (error) => this.onError(error)
    )
  }
  loadAll() {
    this.path = '/user/add';
    this.user= new Utilisateur();
    this.loadProfile();
    this.loadUsers();
  }


  ngOnInit(): void {

    this.loadAll();
  }

  onUserAddSuccess(res) {
    console.log(res);
    this.loadUsers();
    this.isInit = false;
  }

  add() {
    this.user.roles = this.getRoleObj(this.roles);
    console.log(this.user);
    this.service.addUser(this.user, this.path)
    .subscribe(
      (res) => this.onUserAddSuccess(res),
      (error) => this.onError(error)
    )
  }

  delete(obj) {
    this.service.delete(obj)
    .subscribe(
      (res) => this.onDeleteSuccess(res),
      (error) => this.onError(error)
    )
  }
  onDeleteSuccess(res) {
    console.log(res);
    this.loadUsers();
  }

  open(obj: Utilisateur) {
    this.user = obj;
    this.roles = this.getRolesId(this.user.roles);
    let d = this.dialog.open(UtilisateurDialogComponent, {
      width: '95%',
      position: { top: '3%', left: '18%'},
      data: {user: this.user, profile: this.profiles,isInit: this.isInit, path:this.path, roles: this.roles}
    })
    d.afterClosed().subscribe(result => {
       this.loadUsers();
       if(result == true) {
        this.messageService.add({severity:'success', summary: 'Success', detail: 'Opération effectuée avec succès'});
      }
    });
  }

  cancel() {
      this.user = new Utilisateur();
  }

  getPriles() {
    let roles = '';
    for(let h of this.user.roles) {
      roles = roles + h.name + ', ';
    }
    roles = roles.substr(0, roles.length-1);
    return roles;
  }

  init(obj: Utilisateur) {
    this.path = '/user/init';
    this.user = obj;
    this.roles = this.getRolesId(this.user.roles);
    this.isInit = true;
    let d = this.dialog.open(UtilisateurDialogComponent, {
      width: 'auto',
      data: {user: this.user, profile: this.profiles,isInit: this.isInit, path:this.path, roles: this.roles}
    })
    d.afterClosed().subscribe(result => {
      this.loadUsers();
      if(result == true) {
        this.messageService.add({severity:'success', summary: 'Success', detail: 'Opération effectuée avec succès'});
      }
   });
  }

}
