import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfractiondialogComponent } from './infractiondialog.component';

describe('InfractiondialogComponent', () => {
  let component: InfractiondialogComponent;
  let fixture: ComponentFixture<InfractiondialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfractiondialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfractiondialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
