import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cadreJuridique'
})
export class CadreJuridiquePipe implements PipeTransform {

  cadreJuridique= [
    {code: 'enquete_preliminaire', label: 'Enquete préliminaire'},
    {code: 'flag_delit', label: 'Flagrant délit'},
    {code: 'comission_regatoire', label: 'Comission régatoire'}
  ];

  transform(value: any, ...args: unknown[]): unknown {
    let res = null;

    res = this.cadreJuridique.filter(x => x.code == value)
    return res[0].label;
  }

}
