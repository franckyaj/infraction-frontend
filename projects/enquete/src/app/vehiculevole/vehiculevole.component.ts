import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { VehiculeVole } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-vehiculevole',
  templateUrl: './vehiculevole.component.html',
  styleUrls: ['./vehiculevole.component.css'],
  providers: [MessageService]
})
export class VehiculevoleComponent implements OnInit {

  vehicule: VehiculeVole;
  vehicules: VehiculeVole[];
  proprietaireisVictime = false;
  cuti: any;
  type: any;

  etat = [
    { code: 'recherche', label: 'Recherché' },
    { code: 'saisi', label: 'Saisi' }
  ];

  filterMarques: Observable<any[]>;
  myControl = new FormControl();

  constructor(
    public dialogRef: MatDialogRef<VehiculevoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: EnqueteService,
    private messageService: MessageService
  ) { }
  

  loadAll() {
    this.vehicule = new VehiculeVole();
    this.cuti = JSON.parse(localStorage.getItem('user')).login;
    
    if(this.data.type != null) this.type = this.data.type;

    if(this.data.data != null) this.vehicule = this.data.data;
    this.vehicules = [];
    this.filterMarques = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => name ? this._filter(name) : this.data.obj.slice())
    );
  }

  displayFn(name: any): string {
    return name ? name : '';
  }
  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.data.obj.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit(): void {
    this.loadAll();
  }

  save() {
    this.vehicule.cuti = this.cuti;
    this.vehicules.push(this.vehicule);
    this.dialogRef.close(this.vehicules);

   /* let obj = {
      vehiculeVoles: this.vehicules
    };
    this.service.add(obj, '/vehiculevole/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )*/
  }

  edit() {
    this.vehicule.cuti = this.cuti;
    this.vehicules.push(this.vehicule);
    this.dialogRef.close(this.vehicules);

    let obj = {
      vehiculeVoles: this.vehicules
    };
    this.service.add(obj, '/vehiculevole/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )
  }
  onSaveSuccess(res) {
    this.dialogRef.close(res)
  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  cancel() {
    this.vehicule = new VehiculeVole();
    this.vehicules = [];    
    this.dialogRef.close();
  }

  addNew() {
    this.vehicule.cuti = this.cuti;
    this.vehicules.push(this.vehicule);
    this.vehicule = new VehiculeVole();
    console.log(this.vehicules);
  }

  closeDialog() {
    this.dialogRef.close(this.vehicule)
  }


  onDelete(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:46000, detail: 'Opération effectuée avec succès!!!'});
    this.dialogRef.close(res);

  }

  delete() {
    this.service.delete('/vehiculevole/delete/'+this.data.data.id)
    .subscribe(
      (res) => this.onDelete(res),
      (error) => this.onError(error)
    )
  }


}
