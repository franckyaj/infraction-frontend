import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { EnqueteService } from '../enquete.service';

@Component({
  selector: 'app-unite',
  templateUrl: './unite.component.html',
  styleUrls: ['./unite.component.css']
})
export class UniteComponent implements OnInit {

  delegations = [];
  delegation: any;
  tmpdelegation: any;
  unite: any;
  uniteDatas: any;
  id: any;

  constructor(
    public dialogRef: MatDialogRef<UniteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageService: MessageService,
    private service: EnqueteService
  ) { }

  ngOnInit(): void {
    this.loadUnites();
    this.id = null;
    if(this.data != null) {
      this.tmpdelegation = this.data.delegation;
    this.unite = this.data.unite;
    this.id = this.data.id;
    }
  }

  loadUnites() {
    let path = '/localite/unite'
    this.service.listDatas(path)
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }
  onLoadSuccess(res) {
    this.uniteDatas = res.unites;
    let tmp = [];
    for(let r of res.unites) {
      tmp.push(r.delegation);
    }
    this.delegations = tmp.filter((value, index, categoryArray) => categoryArray.indexOf(value) === index);
  }
  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:4600, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  save() {
    if(this.delegation == null) this.delegation = this.tmpdelegation;
    let obj = {
      delegation: this.delegation,
      unite: this.unite,
      id: this.id
    }
    this.dialogRef.close(obj);

  }

  cancel() {
    this.dialogRef.close(null);
  }

}
