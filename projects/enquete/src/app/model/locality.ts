export class Locality {

    region: any;
    departement: any;
    arrondissement: any;
    unite: String;
    delegation: any;
}

export class Unite {

    delegation: any;
    unite: any;
}

export class ParamInfraction {
    categorieInfraction: any;
    designationInfraction: any;
    posteInfraction: any;

}
