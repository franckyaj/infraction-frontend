import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { EnqueteService } from '../enquete.service';
import { ParamInfraction } from '../model/locality';

@Component({
  selector: 'app-edit-param-infraction',
  templateUrl: './edit-param-infraction.component.html',
  styleUrls: ['./edit-param-infraction.component.css']
})
export class EditParamInfractionComponent implements OnInit {

  type = 'EDIT';
  param: ParamInfraction;
  categories: any[];
  categorieInfraction: any;

  constructor(
    public dialogRef: MatDialogRef<EditParamInfractionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageService: MessageService,
    private service: EnqueteService
  ) { }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll() {
    this.param = new ParamInfraction();
    
    if(this.data.obj == null) this.type = 'ADD';
    if(this.data.categories != null) {
      this.categories = this.data.categories;
      this.categories.push('AUTRE');
    }
    if(this.data.obj != null) {
      this.param = this.data.obj;
      this.categorieInfraction = this.param.categorieInfraction;
    }


  }

  save() {
    if(this.categorieInfraction != 'AUTRE') this.param.categorieInfraction = this.categorieInfraction;
    
    this.param.categorieInfraction = this.param.categorieInfraction.toUpperCase();
    this.param.designationInfraction = this.param.designationInfraction.toUpperCase();
    this.param.posteInfraction = this.param.posteInfraction.toUpperCase();
    this.param.active = true;
    this.service.addPramInfraction(this.param)
    .subscribe(
      (res) => this.onSuccess(res),
      (error) => this.onError(error)
    )
  }

  delete() {

  }

  cancel() {
    this.dialogRef.close(3);
  }

  onSuccess(res) {
    this.dialogRef.close(1);
  }

  onError(error) {
    this.dialogRef.close(0);
  }


}
