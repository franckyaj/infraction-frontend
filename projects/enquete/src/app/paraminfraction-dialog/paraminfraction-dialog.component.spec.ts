import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParaminfractionDialogComponent } from './paraminfraction-dialog.component';

describe('ParaminfractionDialogComponent', () => {
  let component: ParaminfractionDialogComponent;
  let fixture: ComponentFixture<ParaminfractionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParaminfractionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParaminfractionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
