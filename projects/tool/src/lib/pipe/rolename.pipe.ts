import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rolename'
})
export class RolenamePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    let result = [];
    for(let i of value) {
      result.push(i.roleName ? i.roleName.toUpperCase() : i.roleName);
    }
    return result;
  }

}
