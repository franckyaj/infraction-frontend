import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleSharedModule } from 'projects/module/src/app/app.module';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'login',
    component: HomeComponent
  },
  //lazy loading module project
  {
    path: 'module',
    loadChildren: () => import('../../../../projects/module/src/app/app.module').then(m => m.ModuleSharedModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
 ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash:true}),
   ModuleSharedModule.forRoot() //lazy loading module project
  ],  exports: [RouterModule]
})
export class AppRoutingModule { }
