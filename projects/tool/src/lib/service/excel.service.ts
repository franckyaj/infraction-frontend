import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  victime_header = [
    {field: 'num', header: 'N'},
    {field: 'nom', header: 'Nom'},
    {field: 'dateNaissance', header: 'Date de naissance'},
    {field: 'lieuNaissance', header: 'Lieu de naissance'},
    {field: 'sexe', header: 'Sexe'},
    {field: 'nationalite', header: 'Nationalité'},
    {field: 'etat', header: 'Etat'},
    {field: 'adresse', header: 'Adresse'},
    {field: 'telephone', header: 'Téléphone'},
    {field: 'phone', header: 'Téléphone'}
  ];

  stats_enquete_region = [
    {field: 'region', header: 'Region'},
    {field: 'plainte', header: 'Plaintes'},
    {field: 'initiative_service', header: 'Initiative service'},
    {field: 'intruction_parquet', header: 'Infraction parquet'},
    {field: 'denonciation', header: 'Denonciation'},
    {field: 'comission_derogatoire', header: 'Comission rogatoire'},
    {field: 'total', header: 'Total'}
  ]
  stats_enquete_dept = [
    {field: 'departement', header: 'Departements'},
    {field: 'plainte', header: 'Plaintes'},
    {field: 'initiative_service', header: 'Initiative service'},
    {field: 'intruction_parquet', header: 'Infraction parquet'},
    {field: 'denonciation', header: 'Denonciation'},
    {field: 'comission_derogatoire', header: 'Comission rogatoire'},
    {field: 'total', header: 'Total'}
  ]

  stats_enquete_arr = [
    {field: 'arrondissement', header: 'Arrondissements'},
    {field: 'plainte', header: 'Plaintes'},
    {field: 'initiative_service', header: 'Initiative service'},
    {field: 'intruction_parquet', header: 'Infraction parquet'},
    {field: 'denonciation', header: 'Denonciation'},
    {field: 'comission_derogatoire', header: 'Comission rogatoire'},
    {field: 'total', header: 'Total'}
  ]
  stats_enquete_del = [
    {field: 'del', header: 'Delegation régionale'},
    {field: 'plainte', header: 'Plaintes'},
    {field: 'initiative_service', header: 'Initiative service'},
    {field: 'intruction_parquet', header: 'Infraction parquet'},
    {field: 'denonciation', header: 'Denonciation'},
    {field: 'comission_derogatoire', header: 'Comission rogatoire'},
    {field: 'total', header: 'Total'}
  ]
  stats_enquete_unit = [
    {field: 'unit', header: 'Unité'},
    {field: 'plainte', header: 'Plaintes'},
    {field: 'initiative_service', header: 'Initiative service'},
    {field: 'intruction_parquet', header: 'Infraction parquet'},
    {field: 'denonciation', header: 'Denonciation'},
    {field: 'comission_derogatoire', header: 'Comission rogatoire'},
    {field: 'total', header: 'Total'}
  ]

  stats_infraction = [
    {field: 'infraction', header: 'Designation infraction'},
    {field: 'fait', header: 'Faits constatés'},
    {field: 'mgav', header: 'GAV -48H'},
    {field: 'pgav', header: 'GAV +48H'},
    {field: 'gav', header: 'Aucune GAV'},
    {field: 'libere', header: 'Libéré'},
    {field: 'defere', header: 'Déféré'},
    {field: 'enfuite', header: 'En fuite'},
    {field: 'camer', header: 'Camerounais'},
    {field: 'etr', header: 'Etranger'},
    {field: 'fMin', header: 'Femme -18 ans'},
    {field: 'mMiM', header: 'Homme -18 ans'},
    {field: 'majF', header: 'Femme +18 ans'},
    {field: 'majM', header: 'Homme +18 ans'},
    {field: 'xmajF', header: 'Femme age inconnu'},
    {field: 'xmajM', header: 'Homme age inconnu'}
  ]

  stats_enquete_unite = [
    {field: 'unite', header: 'Unité de police ayant diligeanté la procédure'},
    {field: 'plainte', header: 'Plaintes'},
    {field: 'initiative_service', header: 'Initiative service'},
    {field: 'intruction_parquet', header: 'Infraction parquet'},
    {field: 'denonciation', header: 'Denonciation'},
    {field: 'comission_derogatoire', header: 'Comission rogatoire'},
    {field: 'total', header: 'Total'}
  ]

  miseencause_header = [
    {field: 'num', header: 'N'},
    {field: 'nom', header: 'Nom'},
   // {field: 'dateNaissance', header: 'Date de naissance'},
    {field: 'sexe', header: 'Sexe'},
    {field: 'dateNaissance', header: 'Date de naissance'},
    {field: 'infractions', header: 'Infractions'},
    {field: 'implication', header: 'Implication'},
    {field: 'gav', header: 'Garde à vue'},
    {field: 'dateinfraction', header: 'Date infraction'},
    {field: 'situation', header: 'Situation'},
    {field: 'etat', header: 'Etat'},
   // {field: 'cni', header: 'N CNI'},
    {field: 'alia', header: 'Alias'},
    {field: 'nationalite', header: 'Nationalite'}
  ];

  infraction_header = [
    {field: 'num', header: 'N'},
    {field: 'categorie', header: 'Catégorie'},
    {field: 'infractionCommise', header: 'Infraction comise'},
    {field: 'date', header: 'Date'},
    {field: 'typeSaisie', header: 'Type de saisie'},
    {field: 'adressePrecise', header: 'Village (ou Quartier)'},
    {field: 'region', header: 'Région'},
    {field: 'departement', header: 'Département'},
    {field: 'arrondissement', header: 'Arrondissement'}
  ];

  stats_infraction_unite = [
   // {field: 'num', header: 'N'},
   {field: 'unite', header: 'Unité de police ayant diligeanté la procédure'},
   {field: 'fait', header: 'Faits constatés'},
   {field: 'mgav', header: 'GAV -48H'},
   {field: 'pgav', header: 'GAV +48H'},
   {field: 'gav', header: 'Aucune GAV'},
   {field: 'libere', header: 'Libéré'},
   {field: 'defere', header: 'Déféré'},
   {field: 'enfuite', header: 'En fuite'},
   {field: 'camer', header: 'Camerounais'},
   {field: 'etr', header: 'Etranger'},
   {field: 'fMin', header: 'Femme -18 ans'},
   {field: 'mMiM', header: 'Homme -18 ans'},
   {field: 'majF', header: 'Femme +18 ans'},
   {field: 'majM', header: 'Homme +18 ans'}
  ];

  stats_infraction_dept = [
    // {field: 'num', header: 'N'},
    {field: 'dept', header: 'Departement'},
    {field: 'fait', header: 'Faits constatés'},
    {field: 'mgav', header: 'GAV -48H'},
    {field: 'pgav', header: 'GAV +48H'},
    {field: 'gav', header: 'Aucune GAV'},
    {field: 'libere', header: 'Libéré'},
    {field: 'defere', header: 'Déféré'},
    {field: 'enfuite', header: 'En fuite'},
    {field: 'camer', header: 'Camerounais'},
    {field: 'etr', header: 'Etranger'},
    {field: 'fMin', header: 'Femme -18 ans'},
    {field: 'mMiM', header: 'Homme -18 ans'},
    {field: 'majF', header: 'Femme +18 ans'},
    {field: 'majM', header: 'Homme +18 ans'}
   ];

   stats_infraction_arr = [
    // {field: 'num', header: 'N'},
    {field: 'arr', header: 'Arrondissement'},
    {field: 'fait', header: 'Faits constatés'},
    {field: 'mgav', header: 'GAV -48H'},
    {field: 'pgav', header: 'GAV +48H'},
    {field: 'gav', header: 'Aucune GAV'},
    {field: 'libere', header: 'Libéré'},
    {field: 'defere', header: 'Déféré'},
    {field: 'enfuite', header: 'En fuite'},
    {field: 'camer', header: 'Camerounais'},
    {field: 'etr', header: 'Etranger'},
    {field: 'fMin', header: 'Femme -18 ans'},
    {field: 'mMiM', header: 'Homme -18 ans'},
    {field: 'majF', header: 'Femme +18 ans'},
    {field: 'majM', header: 'Homme +18 ans'}
   ];

   stats_infraction_region = [
    // {field: 'num', header: 'N'},
    {field: 'region', header: 'Region'},
    {field: 'fait', header: 'Faits constatés'},
    {field: 'mgav', header: 'GAV -48H'},
    {field: 'pgav', header: 'GAV +48H'},
    {field: 'gav', header: 'Aucune GAV'},
    {field: 'libere', header: 'Libéré'},
    {field: 'defere', header: 'Déféré'},
    {field: 'enfuite', header: 'En fuite'},
    {field: 'camer', header: 'Camerounais'},
    {field: 'etr', header: 'Etranger'},
    {field: 'fMin', header: 'Femme -18 ans'},
    {field: 'mMiM', header: 'Homme -18 ans'},
    {field: 'majF', header: 'Femme +18 ans'},
    {field: 'majM', header: 'Homme +18 ans'}
   ];

   stats_infraction_del = [
    // {field: 'num', header: 'N'},
    {field: 'del', header: 'Delegation régionale'},
    {field: 'fait', header: 'Faits constatés'},
    {field: 'mgav', header: 'GAV -48H'},
    {field: 'pgav', header: 'GAV +48H'},
    {field: 'gav', header: 'Aucune GAV'},
    {field: 'libere', header: 'Libéré'},
    {field: 'defere', header: 'Déféré'},
    {field: 'enfuite', header: 'En fuite'},
    {field: 'camer', header: 'Camerounais'},
    {field: 'etr', header: 'Etranger'},
    {field: 'fMin', header: 'Femme -18 ans'},
    {field: 'mMiM', header: 'Homme -18 ans'},
    {field: 'majF', header: 'Femme +18 ans'},
    {field: 'majM', header: 'Homme +18 ans'}
   ];

  enquete_header = [
    {field: 'num', header: 'N'},
    {field: 'referenceProcedure', header: 'Refrence procédure'},
    {field: 'parquetCompetant', header: 'Parquet correspondant'},
    {field: 'affaire', header: 'Affaire'},
    {field: 'cadreJuridique', header: 'Cadre juridique'},
    {field: 'coordonneesOpj', header: 'Coordonnées OPJ'},
    {field: 'resumeFaits', header: 'Résumé des faits'},
    {field: 'victime', header: 'Victimes'},
    {field: 'miseencause', header: 'Mise en cause'},
    {field: 'categorie', header: 'Categorie infraction'},
    {field: 'infractionCommise', header: 'Infraction comise'}
  ];

  vehiculevole_header = [
    {field: 'num', header: 'N'},
    {field: 'marque', header: 'Marque'},
    {field: 'immatriculation', header: 'Immatriculation'},
    {field: 'chasis', header: 'Chasis'},
    {field: 'etat', header: 'Etat'},
    {field: 'proprietaire', header: 'Propriétaire'}
  ];
  stupefiant_header = [
    {field: 'num', header: 'N'},
    {field: 'nature', header: 'Nature'},
    {field: 'quantite', header: 'Quantité'},
    {field: 'provenance', header: 'Provenance'},
    {field: 'destinattion', header: 'Destination'},
    {field: 'etat', header: 'Etat'}
  ];

  cadreJuridique= [
    {code: 'enquete_preliminaire', label: 'Enquete préliminaire'},
    {code: 'flag_delit', label: 'Flagrant délit'},
    {code: 'comission_regatoire', label: 'Comission régatoire'}
  ];

  constructor(
    private datePipe: DatePipe
  ) { }

  generateExcelFile(datas, title, header, date) {
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet(title+' Data');

    let titleRow = worksheet.addRow([title]);
    titleRow.font = { name: 'Comic Sans MS', family: 4, size: 16, underline: 'double', bold: true };
    worksheet.addRow([]);
   // let subTitleRow = worksheet.addRow(['Date : ' + this.datePipe.transform(new Date(), 'medium')]);
    let subTitleRow = worksheet.addRow(['Date : ' + date]);

    let headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });

    datas.forEach(d => {
      let row = worksheet.addRow(d);
      let qty = row.getCell(5);
      let color = 'FF99FF99';
      if (+qty.value < 500) {
        color = 'FF9999'
      }
    
      qty.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: color }
      }
    }
    
    );

    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title+'.xlsx');
});
  }


  getHeaderValue(code, type?) {
    let header = [];
    if(code == 'victime') {
      for(let i of this.victime_header) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'miseencause' || code == 'search-miseencause') {
      for(let i of this.miseencause_header) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'infraction') {
      for(let i of this.infraction_header) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stat_infraction_unit') {
      for(let i of this.stats_infraction_unite) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stat_infraction_del') {
      for(let i of this.stats_infraction_del) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stat_infraction_arr') {
      for(let i of this.stats_infraction_arr) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stat_infraction_dept') {
      for(let i of this.stats_infraction_del) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stat_infraction_region') {
      for(let i of this.stats_infraction_region) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stats_enquete_region') {
      for(let i of this.stats_enquete_region) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'stats_infraction') {
      for(let i of this.stats_infraction) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'stats_enquete_dept') {
      for(let i of this.stats_enquete_dept) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'stats_enquete_arr') {
      for(let i of this.stats_enquete_arr) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'stats_enquete_del') {
      for(let i of this.stats_enquete_del) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'stats_enquete_unit') {
      for(let i of this.stats_enquete_unit) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'stats_enquete_unite') {
      for(let i of this.stats_enquete_unite) header.push(i.header);
     // if(type == 'pdf') header.shift()
    }
    if(code == 'enquete') {
      for(let i of this.enquete_header) header.push(i.header);
      if(type == 'pdf') {
        header.shift()
        header.splice(5,1);
      }
    }
    if(code == 'vehiculevole') {
      for(let i of this.vehiculevole_header) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    if(code == 'stupefiant') {
      for(let i of this.stupefiant_header) header.push(i.header);
      if(type == 'pdf') header.shift()
    }
    return header;
  }

  getGardeAvue(coee) {
    if(coee == '1') {
      return "Moins de 48 Heures";
    }
    else {
      return "Plus de 48 Heures"
    }
  }
  formatDate(date) {
    return this.datePipe.transform(date, 'dd/MM/yyyy')
  }

  formatDateNeevers(date, neevers) {
    let d = null;
    d = this.datePipe.transform(date, 'dd/MM/yyyy')
    if(date != null && neevers == true) {
      d = this.datePipe.transform(date, 'yyyy');
      return "Née vers " + d;
    }
    else if(date != null && neevers == false) {
      return d;
    }
    else {
      return "Non renseignée"
    }
    
    
  }

  formatNationalite(date) {
    if(date) {
      return 'CAMEEROUNAISE';
    }
    else {
      return 'ETRANGEREE';
    }
  }

  getSexeLabel(sexe) {
    if(sexe == 'M') {
      return "Masculin"
    }
    else {
      return "Féminin";
    }
  }

  getvictime(datas) {
    let res = '';
    for(let i of datas) {
      res = res + i.nom + ' ' + i.prenom;
    }
    return res;
  }

  getCategorie(infraction) {
    return infraction.categorie;
  }
  getInfractionComise(infraction) {
    return infraction.infractionCommises;
  }
getCadreJuridique(code) {
  let result = this.cadreJuridique.filter(i => i.code == code);
  return result[0].label;
}

  setDataView(datas, code) {
    console.log(datas);
    
    let rows = [];
    if(code == 'victime'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
            'num':x,
            'nom': i.nom + ' ' + i.prenom,
            'dateNaissance': this.formatDate(i.dateNaissance),
            'lieuNaissance':i.lieuNaissance,
            'sexe':this.getSexeLabel(i.sexe),
            'nationalite':i.nationalite,
            'etat':i.etat,
            'adresse':i.adresse,
            'telephone':i.telephone
          }
        )
        x++;
      }
    } 
    if(code == 'miseencause' || code == 'search-miseencause'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
             'num':x,
            'nom': i.nom + ' ' + i.prenom,
            'sexe':this.getSexeLabel(i.sexe),
            'dateNaissance': this.formatDateNeevers(i.dateNaissance, i.neevers),
            'infractions':i.infractionCommise,
            'implication':i.implication,
            'gav':this.getGardeAvue(i.gav),
            'dateinfraction': this.formatDate(i.date),
            'situation':i.situation,
            'etat':i.etat,
            'alia': i.alias,
            'nationalite': this.formatNationalite(i.nationalite)
          }
        )
        x++;
      }
    }
    if(code == 'infraction'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
            'num':x,
            'categorie': i.categorie ,
            'infractionCommise': i.infractionCommises,
            'date': this.formatDate(i.date),
            'typeSaisie':i.typeSaisie,
            'adressePrecise':i.adressePrecise,
            'region':i.localite.region,
            'departement':i.localite.departement,
            'arrondissement':i.localite.arrondissement
            
          }
        )
        x++;
      }
    } 
    if(code == 'enquete'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
            'num':x,
            'referenceProcedure': i.referenceProcedure ,
            'parquetCompetant': i.parquetCompetant,
            'affaire': i.affaire,
            //'date': this.formatDate(i.date),
            'cadreJuridique': this.getCadreJuridique(i.cadreJuridique),
            'coordonneesOpj':i.coordonneesOpj,
            'resumeFaits':i.resumeFaits,
            'victime': this.getvictime(i.victimes),
            'miseencause': this.getvictime(i.miseencauses),
            'categorie': this.getCategorie(i.infraction),
            'infractionCommise': this.getInfractionComise(i.infraction)
          }
        )
        x++;
      }
    }
    if(code == 'vehiculevole'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
            'num':x,
            'marque': i.marque ,
            'immatriculation': i.immatriculation,
            'chasis': i.chasis,
            'etat': i.etat,
            'proprietaire': i.proprietaire,
          }
        )
        x++;
      }
    } 
    if(code == 'stupefiant'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
            'num':x,
            'nature': i.nature ,
            'quantite': i.quantite,
            'provenance': i.provenance,
            'destinattion': i.destinattion,
            'etat': i.etat
          }
        )
        x++;
      }
    } 
    return rows
  }

  //pdf
  setDataViewPdf(datas, code) {

    
    let rows = [];
    if(code == 'victime'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
          //  'num':x,
            'nom': i.nom + ' ' + i.prenom,
            'dateNaissance': this.formatDate(i.dateNaissance),
            'lieuNaissance':i.lieuNaissance,
            'sexe':this.getSexeLabel(i.sexe),
            'nationalite':i.nationalite,
            'etat':i.etat,
            'adresse':i.adresse,
            'telephone':i.telephone,
            'phone':i.telephone,
          }
        )
        x++;
      }
    } 
    if(code == 'miseencause' || code == 'search-miseencause'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
           // 'num':x,
            'nom': i.nom + ' ' + i.prenom,
            'sexe':this.getSexeLabel(i.sexe),
            'dateNaissance':this.formatDateNeevers(i.dateNaissance, i.neevers),
            'infractions':i.infractionCommise,
            'implication':i.implication,
            'gav':this.getGardeAvue(i.gav),
            'dateinfraction': this.formatDate(i.date),
            'situation':i.situation,
            'etat':i.etat,
            'alia': i.alias,
            'nationalite': this.formatNationalite(i.nationalite)
          }
        )
        x++;
      }
    }
    if(code == 'infraction'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
           // 'num':x,
            'categorie': i.categorie ,
            'infractionCommise': i.infractionCommises,
            'date': this.formatDate(i.date),
           // 'date':this.getSexeLabel(i.sexe),
            'typeSaisie':i.typeSaisie,
            'adressePrecise':i.adressePrecise,
            'region':i.localite.region,
            'departement':i.localite.departement,
            'arrondissement':i.localite.arrondissement
            
          }
        )
        x++;
      }
    } 
    if(code == 'enquete'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
           // 'num':x,
            'referenceProcedure': i.referenceProcedure ,
            'parquetCompetant': i.parquetCompetant,
            'affaire': i.affaire,
            //'date': this.formatDate(i.date),
            'cadreJuridique': this.getCadreJuridique(i.cadreJuridique),
            'coordonneesOpj':i.coordonneesOpj,
           // 'resumeFaits':i.resumeFaits,
            'victime': this.getvictime(i.victimes),
            'miseencause': this.getvictime(i.miseencauses),
            'categorie': this.getCategorie(i.infraction),
            'infractionCommises': this.getInfractionComise(i.infraction)
          }
        )
        x++;
      }
    }
  
    if(code == 'vehiculevole'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
           // 'num':x,
            'marque': i.marque ,
            'immatriculation': i.immatriculation,
            'chasis': i.chasis,
            'etat': i.etat,
            'proprietaire': i.proprietaire,
          }
        )
        x++;
      }
    } 
    if(code == 'stupefiant'){
      let x = 1;
      for(let i of datas) {
        rows.push(
          {
           // 'num':x,
            'nature': i.nature ,
            'quantite': i.quantite,
            'provenance': i.provenance,
            'destinattion': i.destinattion,
            'etat': i.etat
          }
        )
        x++;
      }
    } 
    return rows
  }
}
