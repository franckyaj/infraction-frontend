import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToolService } from 'projects/tool/src/public-api';
import { BehaviorSubject } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  inReport = false;

  constructor(
    private router: Router,
    private tool: ToolService
  ) { }



   myJs1() {
    if( $(".show1").css('display') == 'none'){
      $(".show1").slideDown()
    }
    else {
      $(".show1").slideUp()
    }   
}

  ngOnInit(): void {

    this.tool.inReportObs.subscribe(
      (res) => {
        this.inReport = res;
      }
    )
    
  }

}
