import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Infraction } from '../model/enquete';
import { Locality } from '../model/locality';
import { EnqueteService } from '../service/enquete.service';

@Component({
  selector: 'app-infractiondialog',
  templateUrl: './infractiondialog.component.html',
  styleUrls: ['./infractiondialog.component.css'],
  providers: [MessageService]
})
export class InfractiondialogComponent implements OnInit {

  infraction: Infraction;
  locality: Locality;
  filterUnites: Observable<any[]>;
  regions = [];
  cuti: any;
  myControl = new FormControl();
  departements = [];
  arrondissements = [];
  infractions = [];
  categorieInfraction: [];
  typeSaisie = [
    {code: 'plainte', label: 'Plainte'},
    {code: 'intruction_parquet', label: 'Instruction du parquet'},
    {code: 'comission_derogatoire', label: 'Comission dérogatoire'},
    {code: 'denonciation', label: 'Dénonciation'},
    {code: 'initiative_service', label: 'Initiaive service'},
  ];
  infractionCommises: any;
  type: any;

  constructor(
    private service: EnqueteService,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    public dialogRef: MatDialogRef<InfractiondialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  loadDatas() {
    this.spinner.show();
    this.service.filterTool()
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  loadAll() {
    this.cuti = JSON.parse(localStorage.getItem('user')).login;

    if(this.data.type != null) this.type = this.data.type;

    this.infraction = new Infraction();
    this.locality = new Locality();
    if(this.data.data != null) {
      this.infraction = this.data.data;
    }
    this.loadDatas();
  }
  ngOnInit(): void {
    this.loadAll();
  }

  onLoadSuccess(res) {
    this.regions = res.regions;
    this.infractions = res.categories;
    this.locality = this.data.data.localite;
    this.getDepartement(this.locality.region);
    this.getInfractions(this.data.data.categorie);
  }

  onError(error) {

  }



  save() {
    this.infraction.infractionCommises = [];
    this.infraction.cuti = this.cuti;
    this.infraction.infractionCommises.push(this.infractionCommises);
      this.service.updateObject(this.infraction, '/infraction/save')
      .subscribe(
        (res) => this.onSaveSucces(res),
        (error) => this.onError(error)
      )
  }

  delete() {
    this.service.delete('/infraction/delete/'+this.data.data.id)
    .subscribe(
      (res) => this.onDelete(res),
      (error) => this.onError(error)
    )
  }

  onDelete(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:46000, detail: 'Opération effectuée avec succès!!!'});
    this.dialogRef.close(true);
  }

  onSaveSucces(res) {
    this.dialogRef.close(res);
  }

  cancel() {

  }

  getInfractions(selectedCategorie) {
    this.spinner.show();
    this.service.getInfractions(selectedCategorie)
    .subscribe(
      (res) => this.onGetInfractions(res),
      (error) => this.onError(error)
    )
  }
 /* getInfractions1(event) {
    let selectedCategorie = event.value;
    console.log('cat ' + selectedCategorie);
    this.spinner.show();
    this.service.getInfractions(selectedCategorie)
    .subscribe(
      (res) => this.onGetInfractions1(res),
      (error) => this.onError(error)
    )
  }*/
  onGetInfractions(res) {
  //  this.infractions = res;
    this.infraction = this.data.data;
    this.infractionCommises = this.data.data.infractionCommises[0];
  }
  onGetInfractions1(res) {
    this.spinner.hide();
  //  this.categorieInfraction = res;
  }

  displayFn(name: any): string {
    return name ? name : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.regions.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  getArrondissement(selectedDept) {
    this.arrondissements= [];
    this.service.getRegionsDates(selectedDept, 'arrondissement')
    .subscribe(
      (res) => this.onArrdonsieement(res),
      (error) => this.onError(error)
    )
  }
  getArrondissement1(event) {
    this.arrondissements= [];
    let selectedDept = event.value;
    this.service.getRegionsDates(selectedDept, 'arrondissement')
    .subscribe(
      (res) => this.onArrdonsieement1(res),
      (error) => this.onError(error)
    )
  }
  getDepartement(selectedRegion) {
    this.spinner.show();
    this.departements= [];
    this.arrondissements= [];
    this.service.getRegionsDates(selectedRegion, 'departement')
    .subscribe(
      (res) => this.onDeptSuccess(res),
      (error) => this.onError(error)
    )
  }
  getDepartement1(event) {
    
    this.spinner.show();
    let selectedRegion = event.value
    this.departements= [];
    this.arrondissements= [];
    this.service.getRegionsDates(selectedRegion, 'departement')
    .subscribe(
      (res) => this.onDeptSuccess1(res),
      (error) => this.onError(error)
    )
  }
  onDeptSuccess(res) {
    this.departements = res[0];
    this.locality = this.data.data.localite;
    this.getArrondissement(this.locality.departement)
  }
  onDeptSuccess1(res) {
    this.spinner.hide();
    this.departements = res[0];
  }
  onArrdonsieement(res) {
    this.arrondissements = res[0];
    this.locality = this.data.data.localite;
    this.getUnite(this.locality.arrondissement)
  }
  onArrdonsieement1(res) {
    this.arrondissements = res;
  }
  getUnite(event) {
    this.spinner.hide();
    this.locality = this.data.data.localite;
    this.infraction = this.data.data;
  }
  getUnite1(event) {
    this.spinner.hide();
  }

  getInfractions1(event) {
    this.spinner.show();
    let selectedCategorie = event.value;
   // this.infraction.infractionCommises = ;
   // this.infraction.infractionCommises.push(event.value);
    localStorage.setItem('mec_infraction', JSON.stringify(selectedCategorie));
    console.info(JSON.parse(localStorage.getItem('mec_infraction')));
    this.service.getInfractions(selectedCategorie)
    .subscribe(
      (res) => this.onGetInfractions2(res),
      (error) => this.onError(error)
    )

  }
  onGetInfractions2(res){
    this.spinner.hide();

    this.infraction.categorie = res[0];
  }

}
