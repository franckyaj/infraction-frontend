import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StupefiantComponent } from './stupefiant.component';

describe('StupefiantComponent', () => {
  let component: StupefiantComponent;
  let fixture: ComponentFixture<StupefiantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StupefiantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StupefiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
