import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map, take } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  currentuser: any; //objet utilisateur

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isLoggedIn
    .pipe(
      take(1),
    map( (isLoggedIn: boolean) => {
      this.currentuser = JSON.parse(localStorage.getItem('user'));
      console.log('user', this.currentuser);
      if (this.currentuser == null){
        this.router.navigate([`/login`]);
        return false;
      }
      else if(this.currentuser != null){
        if(this.currentuser.defaultUser == false) {
          return true;
        }
        else {
          return false
        }
      }
    }));
 
  
}
  
}
