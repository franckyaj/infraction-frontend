import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MessageService } from 'primeng/api';
import { ToolModule } from 'projects/tool/src/public-api';
import { ModuleWithProviders } from '@angular/compiler/src/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { VictimeComponent } from './victime/victime.component';
import { MiseencauseComponent } from './miseencause/miseencause.component';
import { InfractionComponent } from './infraction/infraction.component';
import { StupefiantComponent } from './stupefiant/stupefiant.component';
import { ArmeComponent } from './arme/arme.component';
import { CoordonneOpjComponent } from './coordonne-opj/coordonne-opj.component';
import { CoordonneAssistantComponent } from './coordonne-assistant/coordonne-assistant.component';
import {MatStepper, MatStepperModule} from '@angular/material/stepper';
import { CdkStepper } from '@angular/cdk/stepper';
import { VehiculevoleComponent } from './vehiculevole/vehiculevole.component';
import { ReportingComponent } from './reporting/reporting.component';
import { ExportComponent } from './export/export.component';
import { OrderModule } from 'ngx-order-pipe';
import { VictimePipe } from './pipe/victime.pipe';
import { MiseencausePipe } from './pipe/miseencause.pipe';
import { ArmePipe } from './pipe/arme.pipe';
import { VehiculePipe } from './pipe/vehicule.pipe';
import { StupefiantPipe } from './pipe/stupefiant.pipe';
import { ParaminfractionComponent } from './paraminfraction/paraminfraction.component';
import { ParaminfractionDialogComponent } from './paraminfraction-dialog/paraminfraction-dialog.component';
import { DateformatPipe } from './pipe/dateformat.pipe';
import { InfractiondialogComponent } from './infractiondialog/infractiondialog.component';
import { EnquetedialogComponent } from './enquetedialog/enquetedialog.component';
import { DatePipe } from '@angular/common';
import { NomPipe } from './pipe/nom.pipe';
import { ChartComponent } from './chart/chart.component';
import {ChartModule} from 'primeng/chart';
import { CreComponent } from './cre/cre.component';
import { StatistiqueComponent } from './statistique/statistique.component';
import { GavPipe } from './pipe/gav.pipe';
import { CadreJuridiquePipe } from './pipe/cadre-juridique.pipe';
import { TypeSaisiePipe } from './pipe/type-saisie.pipe';
import { LibelleComponent } from './libelle/libelle.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    VictimeComponent,
    MiseencauseComponent,
    InfractionComponent,
    StupefiantComponent,
    ArmeComponent,
    CoordonneOpjComponent,
    CoordonneAssistantComponent,
    VehiculevoleComponent,
    ReportingComponent,
    ExportComponent,
    VictimePipe,
    MiseencausePipe,
    ArmePipe,
    VehiculePipe,
    StupefiantPipe,
    ParaminfractionComponent,
    ParaminfractionDialogComponent,
    DateformatPipe,
    InfractiondialogComponent,
    EnquetedialogComponent,
    NomPipe,
    ChartComponent,
    CreComponent,
    StatistiqueComponent,
    GavPipe,
    CadreJuridiquePipe,
    TypeSaisiePipe,
    LibelleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OrderModule,
    ChartModule,
    ToolModule
  ],
  providers: [
    MessageService
  ],
  entryComponents: [
    CoordonneOpjComponent,
    VictimeComponent,
    EnquetedialogComponent,
    MiseencauseComponent,
    InfractiondialogComponent,
    ChartComponent,
    ParaminfractionDialogComponent,
    CoordonneAssistantComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

const providers = []


@NgModule({})
export class EnqueteSharedModule{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}
