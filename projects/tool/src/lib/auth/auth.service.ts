import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ToolService } from '../tool.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(false); // {1}
  

  constructor(
    private router: Router,
    private toolService: ToolService
  ) { }

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  login(res:any) {
    let roles = [];
    this.toolService.userPermission = []; //initialisation des habilitations utilisateurs
    localStorage.setItem('user', JSON.stringify(res));
    for(let i of res.roles) {
      this.toolService.userPermission.push(i.privileges); //chargement des habilitation utilisateurs
    }
    localStorage.setItem('userRoles',JSON.stringify(roles));
    this.loggedIn.next(true);
    this.router.navigate([`/main/home`]);
  }

}
