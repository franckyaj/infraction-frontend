import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { Enquete } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';

@Component({
  selector: 'app-enquetedialog',
  templateUrl: './enquetedialog.component.html',
  styleUrls: ['./enquetedialog.component.css'],
  providers: [MessageService]
})
export class EnquetedialogComponent implements OnInit {

  enquete: Enquete;
  cuti: any;

  constructor(
    public dialogRef: MatDialogRef<EnquetedialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageService: MessageService,
    private service: EnqueteService
  ) { }


  loadAll() {
    this.enquete = new Enquete();
    this.cuti = JSON.parse(localStorage.getItem('user')).login;
    this.enquete = this.data.data;
  }
  ngOnInit(): void {
    this.loadAll();
  }

  save() {
    this.enquete.cuti = this.cuti;
    this.service.add(this.enquete, '/enquete/save')
    .subscribe(
      (res) => this.onAddSuccess(res),
      (error) => this.onError(error)
    )
  }

  delete() {
    this.service.delete('/enquete/delete/'+this.data.data.id)
    .subscribe(
      (res) => this.onAddSuccess(res),
      (error) => this.onError(error)
    )
  }

  onAddSuccess(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:296000, detail: 'Opération effectuée avec succès! ' });
    this.dialogRef.close(true);
  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:56000, detail: 'Error ' + error.message});
  }

  cancel() {

  }

}
