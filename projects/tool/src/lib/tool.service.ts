import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { AppConfig } from './interface/app-config';
import { ShareService } from './service/share.service';

@Injectable({
  providedIn: 'root'
})
export class ToolService {

  isLoading = new BehaviorSubject<boolean>(false);
  inReportObs = new BehaviorSubject<boolean>(false);

  adminPath= 'utilisateur-service';
  enquetePath = 'infraction-service'
  userPermission = [];
  address: any;
  config: AppConfig;
  appconfig: any;


  applicationPermissions = [
    {
      name: 'Gestion des infractions',
      privileges: [
        {code: 'SAISI_FORM', name: 'Saisie d\' une enquète' },
        {code: 'EDIT_DATA', name: 'Modification des données' },
        {code: 'DELETE_DATA', name: 'Suppression des donnée' },
        {code: 'EDIT_ALL_DATA', name: 'Modification de toutes les données' }
      ]
    },
    {
      name: 'Module d\'administration',
      privileges:[
        {code: 'ADMIN_EDIT_USER', name: 'Ajout/Edition d\'utilisateur' },
        {code: 'ADMIN_ADD_PROFIL', name: 'Modification de profil' },
        {code: 'ADMIN_DEL_USER', name: 'Suppression d\'un utilisateur' },
        {code: 'ADMIN_INIT_USER', name: 'Initialisaiton de la session utilisateur'},//
        {code: 'ADMIN_MANG_PERMISSIONS', name: 'Gestion des permissions'},//
        {code: 'MANAGE_PARAM', name: 'Gestion des parametrages'},//
        
      ]
    }
  ];

  applicationPermissionss = [
    {code: 'ADMIN_EDIT_USER', name: 'Ajout/Edition d\'utilisateur' },
    {code: 'ADMIN_ADD_PROFIL', name: 'Edition de profil' },
    {code: 'ADMIN_DEL_USER', name: 'Suppression d\'un utilisateur' },
    {code: 'ADMIN_INIT_USER', name: 'Initialisaiton de la session utilisateur'},
    {code: 'ACCUEIL_ADD_PATIENT', name: 'Ajout d\'un patient  ' },
    {code: 'ACCUEIL_EDIT_PATIENT', name: 'Edition Fil d\'attente' },
    {code: 'ACCUEIL_FIL_ATT', name: 'Accès fil d\'attente' },
    {code: 'ACCUEIL_INFO_PATIENT', name: 'Informations patient' },
    {code: 'ACCUEIL_RDV', name: 'Gestion RDV' },
    {code: 'ACCUEIL_EDIT_RDV', name: 'Ajouter/Modifier/supprimer un RDV '}
  ]

  constructor(
    public dialog: MatDialog,
    private httpClient: HttpClient
  ) { 
    this.getToken();
  }

  getToken() {
    return this.httpClient
      .get<AppConfig>('assets/config/app-config.json')
      .toPromise()
      .then(config =>  {
        this.config = config;
        localStorage.setItem('config', JSON.stringify(config));

        let hostnqme = location.hostname;
        let hosts = config.host.split(';');
        let host = null;

        if(hosts[0].indexOf(hostnqme) > -1) host = hosts[0]
        if(hosts[1].indexOf(hostnqme) > -1) host = hosts[1]
        //if(hosts[2].indexOf(hostnqme) > -1) host = hosts[2]

        console.info('host')
        console.info(host);

        if(host != null) {
          this.address = config.protocole + '://' + host + ':' + config.port; // 'http://localhost:9001'
          this.getTokenFunction();
        }
        else {
          alert('erreur hostname');
        }
        
      } );
  }


  getTokenFunction() {
    
    let config = JSON.parse(localStorage.getItem('config')) 

    let hostnqme = location.hostname;
    let hosts = config.host.split(';');
    let host = null;

    if(hosts[0].indexOf(hostnqme) > -1) host = hosts[0]
    if(hosts[1].indexOf(hostnqme) > -1) host = hosts[1]


    const body = new HttpParams()
    .set('username', JSON.parse(localStorage.getItem('config')).username)
    .set('password', JSON.parse(localStorage.getItem('config')).password)
    .set('grant_type', "password");
    
    const auth ='auth';

    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8')
    .set('Authorization', 'Basic' + auth);



    let url = config.protocole + "://" + host + ":" + config.port;
   
    return new Promise<void>((resolve,reject) =>
      {
        this.httpClient.post(url + "/authorization-service/oauth/token", body, { headers: headers })
        .toPromise()
        .then(
          (res: any) => {
           localStorage.setItem('token', JSON.stringify(res))
          resolve();
          }
        )
        .catch(
          error => {
            console.log('TOKEN FAILED ' );
            console.log(error);
          }
        ) 
      } 
    ) 
  }
}
