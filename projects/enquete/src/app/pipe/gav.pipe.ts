import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gav'
})
export class GavPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if(value == "1") return 'Moins de 48H';
    if(value == "2") return 'Plus de 48H';
    if(value == "3") return 'Pas de GAV';
  }

}
