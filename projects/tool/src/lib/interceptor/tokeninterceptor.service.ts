import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TokeninterceptorsService implements HttpInterceptor{

  constructor(
    private router: Router,
    private permission: NgxPermissionsService,
  ) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
        if (err.status === 401) {
            // auto logout if 401 response returned from api
            this.permission.flushPermissions();
            localStorage.clear();
            this.router.navigate([`/login`]);
            //location.(true);
        }

        const error = err.error.message || err.statusText;
        return throwError(error);
    }))
}

}
