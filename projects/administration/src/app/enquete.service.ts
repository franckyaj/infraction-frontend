import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToolService } from 'projects/tool/src/public-api';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnqueteService {

  
  constructor(
    private http: HttpClient,
    private toolService: ToolService
  ) { }

  add(object, path) {
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + path , object)
    .pipe(map((res: any) => {
      return res;
      } ));
  }



  addPramInfraction(param) {
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.enquetePath + '/param-infraction/add' , param)
    .pipe(map((res: any) => {
      return res;
      } ));
  }


  delete(path) {
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + path , null)
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  list(object, path) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + path , object)
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  //param-infraction
  saveParamsInfraction(object) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.enquetePath + '/param-infraction/savelist' , object)
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  //param-infraction
  deleteParamsInfraction(object) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.enquetePath + '/param-infraction/delete/'+object.id, null)
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  //param-infraction
  getParamsInfraction() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.get(this.toolService.address + '/' + this.toolService.enquetePath + '/param-infraction/list' )
    .pipe(map((res: any) => {
      return res;
      } ));
  }
  saveLocalite(object, path) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.address + '/' + this.toolService.enquetePath + path , object)
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  titleCaseWord(word) {
    if (!word) return word;
    if(word.includes('-')) {
      let res = null;
      let words = word.split('-');
       words[0] = words[0][0].toUpperCase() + words[0].substr(1).toLowerCase();
       if(words[0] == 'Extreme') words[0] = 'Extrême'
       alert(words[0]);
       words[1] = words[1][0].toUpperCase() + words[1].substr(1).toLowerCase();
       return words[0] + '-' + words[1];
      
    }
    else {
      return word[0].toUpperCase() + word.substr(1).toLowerCase();
    }
  }

  getRegionsDates(query,type) {
    let path = '';
    let path2 = null;
    if(type == 'departement') {
      path = 'departement'
      path2 = 'localite/unite/'+query;

      let url1 = this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + '/localite/'+path, {
        libelle: query
      } );
      let region = this.titleCaseWord(query);
      let url2 = this.http.get(this.toolService.enquetePath + '/' + this.toolService.enquetePath + '/localite/unite/region/'+region);

      return forkJoin([url1, url2]);
  
    }
    if(type == 'arrondissement') {
      path = 'arrondissement'
      let url1 = this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + '/localite/'+path, {
        libelle: query
      } );
      return forkJoin([url1, url1]);

    } 

    if(type == 'unite') {
      let url1 = this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + '/localite/unite/delegation', 
      {
        delegation: query
      });
      return forkJoin([url1, url1]);

    } 

  }

  filterTool() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.get(this.toolService.enquetePath + '/' + this.toolService.enquetePath + '/tool/filter' )
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  getInfractions(categorie) {
    let obj = {
      designationInfraction: categorie
    };
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + '/param-infraction/infractions', obj )
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  loadDatas() {
    let getArmes = this.http.post(this.toolService.enquetePath + '/' + this.toolService.adminPath + 'filter/armes' , {}) 
    let getStupefiants = this.http.post(this.toolService.enquetePath + '/' + this.toolService.adminPath + 'filter/stupefiants' , {})
    let getVehicules = this.http.post(this.toolService.enquetePath + '/' + this.toolService.adminPath + 'filter/vehicules' , {});
    let getPersonne = this.http.post(this.toolService.enquetePath + '/' + this.toolService.adminPath + 'filter/personne' , {});

    return forkJoin([getArmes, getStupefiants, getVehicules, getPersonne]);
  }

  listDatas(path) {
    return this.http.get(this.toolService.address + '/' + this.toolService.enquetePath + path )
    .pipe(map((res: any) => {
      return res;
      } ));
  }

  

  updateObject(obj,path) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.toolService.enquetePath + '/' + this.toolService.enquetePath + path, obj )
    .pipe(map((res: any) => {
      return res;
      } ));
  }
}
