import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordonneOpjComponent } from './coordonne-opj.component';

describe('CoordonneOpjComponent', () => {
  let component: CoordonneOpjComponent;
  let fixture: ComponentFixture<CoordonneOpjComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoordonneOpjComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordonneOpjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
