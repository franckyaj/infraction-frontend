import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from 'projects/tool/src/lib/auth/auth.service';
import { ShareService } from 'projects/tool/src/lib/service/share.service';
import { ToolService } from 'projects/tool/src/public-api';
import { Logindto } from '../model/logindto';
import { LoginService } from '../service/login.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MessageService]
})
export class HomeComponent implements OnInit {

  loginDto: Logindto;
  loading: boolean;
  failedLogin = false;
  isDefaultUser = false;
  confirmpassword: String;
  editedPwd = false;


  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private service: LoginService,
    private toolService: ToolService,
    private authService: AuthService,
    private share: ShareService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll() {
    this.loginDto = new Logindto();
    this.getToken();
  }

  getToken() {
   // if(JSON.parse(localStorage.getItem('config')) != null) this.share.getToken();
  }

  login(){
    this.spinner.show();
   this.service.login(this.loginDto)
   .subscribe(
     (res) => this.onLoginSuccess(res),
     (error) => this.onError(error)
   )
  }

  onLoginSuccess(res) {
    this.spinner.hide();
    if(res == null) {
      this.failedLogin = true;
      this.messageService.add({severity:'error', summary: 'Attention',life:6000, detail: 'Login ou mot de passe incorrect'});

    }
    else if(res.active == false) {
      this.failedLogin = true;
      this.messageService.add({severity:'info', summary: 'Information',life:6000, detail: 'Utilisateur non actif, Veuillez contacter votre administrateur'});
    }
    else {
      this.isDefaultUser = res.defaultUser
      if(this.isDefaultUser == false) {
        this.authService.login(res)
      } 
    }
      
  }

  editPwd() {
    if(this.loginDto.password == this.confirmpassword) {
      this.service.editPwd(this.loginDto)
   .subscribe(
     (res) => this.onEditPwdSuccess(res),
     (error) => this.onError(error)
   )
    }
    else {
      alert('Mot de passe saisis incorrect');
    }
  }

  onEditPwdSuccess(res) {
    if(res == true) {
      this.isDefaultUser = false;
      this.editedPwd = true;
      this.loginDto = new Logindto();
    } 
  }

  onError(error) {
    this.spinner.hide();
    this.loading= false;
    console.log(error);
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: error.code + ' '+ error.message});
    location.reload();
  }



}
