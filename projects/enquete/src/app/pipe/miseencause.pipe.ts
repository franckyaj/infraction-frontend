import { Pipe, PipeTransform } from '@angular/core';
import { Enquete, MiseEnCause } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';

@Pipe({
  name: 'miseencause'
})
export class MiseencausePipe implements PipeTransform {

  transform(value: Enquete, ...args: unknown[]): unknown {
    let mecs = value.miseencauses;
    let res = '';
    for(let item of mecs) res = res + ' ' + item.nom + ' ' + item.prenom + ';'
    return res;
  }

}
