import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordonneAssistantComponent } from './coordonne-assistant.component';

describe('CoordonneAssistantComponent', () => {
  let component: CoordonneAssistantComponent;
  let fixture: ComponentFixture<CoordonneAssistantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoordonneAssistantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordonneAssistantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
