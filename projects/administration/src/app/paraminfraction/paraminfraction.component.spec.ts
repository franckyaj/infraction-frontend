import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParaminfractionComponent } from './paraminfraction.component';

describe('ParaminfractionComponent', () => {
  let component: ParaminfractionComponent;
  let fixture: ComponentFixture<ParaminfractionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParaminfractionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParaminfractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
