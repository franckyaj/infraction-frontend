import { Pipe, PipeTransform } from '@angular/core';
import { ToolService } from '../tool.service';

@Pipe({
  name: 'formatrole'
})
export class FormatrolePipe implements PipeTransform {

  constructor(
    private service: ToolService
  ) {}

  transform(value: any, ...args: unknown[]): unknown {
    let result = [];
    for(let v of value) {
      for(let i of this.service.applicationPermissionss) {
        if(v == i.code) result.push(i.name)
      }
    }
    return result;
  }

}
