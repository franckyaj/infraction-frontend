import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Stupefiant } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-stupefiant',
  templateUrl: './stupefiant.component.html',
  styleUrls: ['./stupefiant.component.css'],
  providers: [MessageService]
})
export class StupefiantComponent implements OnInit {

  stupefiant: Stupefiant;
  stupefiants: Stupefiant[];
  cuti: any;
  type: any;


  etat = [
    { code: 'recherche', label: 'Recherché' },
    { code: 'saisi', label: 'Saisi' }
  ];
  filterNatures: Observable<any[]>;
  myControl = new FormControl();

  constructor(
    public dialogRef: MatDialogRef<StupefiantComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageService: MessageService,
    private service: EnqueteService
  ) { }

  loadAll() {
    this.stupefiant = new Stupefiant();
    this.cuti = JSON.parse(localStorage.getItem('user')).login;

    if(this.data.type != null) this.type = this.data.type;

    if(this.data.data != null) this.stupefiant = this.data.data;
    this.stupefiants = [];
    this.filterNatures = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => name ? this._filter(name) : this.data.obj.slice())
    );
  }

  displayFn(name: any): string {
    return name ? name : '';
  }
  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.data.obj.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit(): void {
    this.loadAll();
  }

  save() {
    this.stupefiants.push(this.stupefiant);
    this.dialogRef.close(this.stupefiants);

    /*let obj = {
      stupefiants: this.stupefiants
    };
    this.service.add(obj, '/stupefiant/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )*/
  }

  edit() {
    this.stupefiants.push(this.stupefiant);
    this.dialogRef.close(this.stupefiants);

    let obj = {
      stupefiants: this.stupefiants
    };
    this.service.add(obj, '/stupefiant/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )
  }
  onSaveSuccess(res) {
    this.dialogRef.close(res)
  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }
  
  cancel() {
    this.stupefiant = new Stupefiant();
    this.stupefiants = [];
    console.log(this.stupefiants);
    this.dialogRef.close();
  }


  onDelete(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:46000, detail: 'Opération effectuée avec succès!!!'});
    this.dialogRef.close(res);

  }

  addNew() {
    this.stupefiants.push(this.stupefiant);
    this.stupefiant = new Stupefiant();
  }

  delete() {
    this.service.delete('/stupefiant/delete/'+this.data.data.id)
    .subscribe(
      (res) => this.onDelete(res),
      (error) => this.onError(error)
    )
  }

}
