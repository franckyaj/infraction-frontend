import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { ToolService } from 'projects/tool/src/public-api';
import { ProfilService } from '../profil/profil.service';

@Component({
  selector: 'app-profil-dialog',
  templateUrl: './profil-dialog.component.html',
  styleUrls: ['./profil-dialog.component.css'],
  providers: [MessageService]
})
export class ProfilDialogComponent implements OnInit {

  profils = [];
  profil: any = {};
  habilitations = [];


  constructor(
    public dialogRef: MatDialogRef<ProfilDialogComponent>,
    private service: ProfilService,
    private toolService: ToolService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll() {
    if(this.data != null) {
      this.habilitations = this.data.habilitations;
      this.profil = this.data.profil;
    }
  }

  onAddProfilSuccess(res) {
    this.toolService.isLoading.next(false);
    console.log(res);
    this.profil = {};
    this.dialogRef.close(true);
  }

  cancel() {
    this.dialogRef.close(false);
  }

  onError(error) {
    this.toolService.isLoading.next(false);
    console.log(error);
  }

  add(){
    // this.profil.habilitation = this.getHabilitation();
     console.log(this.profil);
     this.toolService.isLoading.next(true);
     this.service.addProfil(this.profil)
     .subscribe(
       (res) => this.onAddProfilSuccess(res),
       (error) => this.onError(error)
     )
     //this.profils.push(this.profil);
   }

}
