import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/compiler/src/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PermissionComponent } from './permission/permission.component';
import { ProfilComponent } from './profil/profil.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { ToolModule } from 'projects/tool/src/public-api';
import { ProfilDialogComponent } from './profil-dialog/profil-dialog.component';
import { UtilisateurDialogComponent } from './utilisateur-dialog/utilisateur-dialog.component';
import { ExportComponent } from './export/export.component';
import { ParaminfractionComponent } from './paraminfraction/paraminfraction.component';
import { EditParamInfractionComponent } from './edit-param-infraction/edit-param-infraction.component';
import { UniteComponent } from './unite/unite.component';
import { LocaliteComponent } from './localite/localite.component';

@NgModule({
  declarations: [
    AppComponent,
    PermissionComponent,
    ProfilComponent,
    UtilisateurComponent,
    HomeComponent,
    NavComponent,
    ProfilDialogComponent,
    UtilisateurDialogComponent,
    ExportComponent,
    ParaminfractionComponent,
    EditParamInfractionComponent,
    UniteComponent,
    LocaliteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToolModule
  ],
  entryComponents: [
    ProfilDialogComponent,
    EditParamInfractionComponent,
    UniteComponent,
    LocaliteComponent,
    UtilisateurDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

const providers = []


@NgModule({})
export class AdministrationSharedModule{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}
