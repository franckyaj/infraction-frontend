import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculevoleComponent } from './vehiculevole.component';

describe('VehiculevoleComponent', () => {
  let component: VehiculevoleComponent;
  let fixture: ComponentFixture<VehiculevoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiculevoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculevoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
