import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { EnqueteService } from '../enquete.service';
import { ParamInfraction } from '../model/locality';
import * as XLSX from 'xlsx';
import { EditParamInfractionComponent } from '../edit-param-infraction/edit-param-infraction.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-paraminfraction',
  templateUrl: './paraminfraction.component.html',
  styleUrls: ['./paraminfraction.component.css']
})
export class ParaminfractionComponent implements OnInit {


  param: ParamInfraction;
  params: ParamInfraction[];
  datas = [];
  isLoad = false;
  categories: any[];

  constructor(
    private service: EnqueteService,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.param = new ParamInfraction();
    this.params = [];
    this.loadAll();
  }

  loadAll() {
    this.getInfractions();
  }

  getInfractions() {
    this.spinner.show();
    this.service.getParamsInfraction()
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  onLoadSuccess(res) {
    this.spinner.hide();
    this.params = res;
    this.categories = [];
    if(this.params.length > 0) this.isLoad = true;
    for(let i of this.params) {
      this.categories.push(i.categorieInfraction);
    }
  }

  onFileChange(ev) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
     // document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
      this.saveData(JSON.parse(dataString).Sheet1);
    }
    reader.readAsBinaryString(file);

  }

  saveData(datas) {
    for(let i of datas) {
      this.param = new ParamInfraction();
      let keys = Object.keys(i);
      let entries = Object.entries(i);
      for(const [prop,  val] of entries ) {
        if(prop == 'DESIGNATION INFRACTIONS') this.param.designationInfraction = val;
        if(prop == 'Catégorie infraction') this.param.categorieInfraction = val;
        if(prop == 'Poste Infraction') this.param.posteInfraction = val;
      }
      this.param.active = true;
      this.params.push(this.param);
    
    }
    if(this.params.length > 0) this.isLoad = false;

  }

  saveDatas() {
    this.spinner.show();

    let obj = {
      paramInfractions: this.params
    }
    this.service.saveParamsInfraction(obj)
    .subscribe(
      (res) => this.onSuccess(res),
      (error) => this.onError(error)
    )
  }

  onSuccess(res) {
    this.spinner.hide();
    this.messageService.add({severity:'success', summary: 'Information',life:296000, detail: 'Sauvegarde effectuée avec succès!!'});

  }

  onError(error) {
    this.spinner.hide();
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  add(obj?) {
    let dialogRef = this.dialog.open(EditParamInfractionComponent, {
      disableClose: false,
      width: '90%',
      data: {obj: obj, categories: this.categories}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        console.log(result);
      }
    )
  }

  delete(obj) {
    this.service.deleteParamsInfraction(obj)
    .subscribe(
      (res) => this.onDelSuccess(res, obj),
      (error) => this.onError(error)
    )
  }

  onDelSuccess(res, obj) {
    delete this.params[obj];
    this.messageService.add({severity:'success', summary: 'Information',life:296000, detail: 'Sauvegarde effectuée avec succès!!'});
  }

}
