import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { MenuItem } from 'primeng/api';
import { ToolService } from 'projects/tool/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'enquete';
  username = '';
  loading = false

  items: MenuItem[];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private permission: NgxPermissionsService,
    private toolService: ToolService,
  ){}


  ngOnInit() {
    this.loadPermission();
    this.username = JSON.parse(localStorage.getItem('user')).firstName.toUpperCase() + ' '+ JSON.parse(localStorage.getItem('user')).lastName;
    this.items = [
      {label: 'Quitter', icon: 'pi pi-times', routerLink: ['/main/home']},
      {separator: true},
      {label: 'Se deconnecter', icon: 'pi pi-sign-out', command: () =>{
        this.logout()
      }},
  ];
  
  }

  loadPermission() {
    if(this.toolService.userPermission.length == 0) {
      this.toolService.userPermission = JSON.parse(localStorage.getItem('role'));
      this.permission.loadPermissions(this.toolService.userPermission);
    }
  }


  logout() {
    this.permission.flushPermissions();
    localStorage.clear();
    this.dialog.closeAll();
    this.router.navigate([`/login`]);
    location.reload();

  }
}
