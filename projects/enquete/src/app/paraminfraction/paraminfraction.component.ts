import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ParamInfraction } from '../model/locality';
import { EnqueteService } from '../service/enquete.service';
import * as XLSX from 'xlsx';
import { MatDialog } from '@angular/material/dialog';
import { ParaminfractionDialogComponent } from '../paraminfraction-dialog/paraminfraction-dialog.component';


@Component({
  selector: 'app-paraminfraction',
  templateUrl: './paraminfraction.component.html',
  styleUrls: ['./paraminfraction.component.css'],
  providers: [MessageService]
})
export class ParaminfractionComponent implements OnInit {

  
  param: ParamInfraction;
  params: ParamInfraction[];

  constructor(
    private service: EnqueteService,
    private dialog: MatDialog,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.param = new ParamInfraction();
    this.params = [];
  }

  onFileChange(ev) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
     // document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
      this.saveData(JSON.parse(dataString).Sheet1);
    }
    reader.readAsBinaryString(file);

  }

  saveData(datas) {
    for(let i of datas) {
      this.param = new ParamInfraction();
      let keys = Object.keys(i);
      let entries = Object.entries(i);
      for(const [prop,  val] of entries ) {
        if(prop == 'DESIGNATION INFRACTIONS') this.param.designationInfraction = val;
        if(prop == 'Catégorie infraction') this.param.categorieInfraction = val;
        if(prop == 'Poste Infraction') this.param.posteInfraction = val;
      }
      this.params.push(this.param);
    }

  }

  saveDatas() {
    let obj = {
      paramInfractions: this.params
    }
    this.service.saveParamsInfraction(obj)
    .subscribe(
      (res) => this.onSuccess(res),
      (error) => this.onError(error)
    )
  }

  onSuccess(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:296000, detail: 'Sauvegarde effectuée avec succès!!'});

  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  add() {
    let dialogRef = this.dialog.open(ParaminfractionDialogComponent, {
      disableClose: false,
      width: '50%',
    })
    dialogRef.afterClosed().subscribe(
      result => {
        console.log(result);
      }
    )
  }

}
