import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Victime } from '../model/enquete';
import { EnqueteService } from '../service/enquete.service';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-victime',
  templateUrl: './victime.component.html',
  styleUrls: ['./victime.component.css'],
  providers: [MessageService]
})
export class VictimeComponent implements OnInit {

  victime: Victime;
  victimes: Victime[];
  cuti: any;

  typeSexe= [
    { code: 'M', label: 'Masculin' },
    { code: 'F', label: 'Feminin' },
  ];

  nationality = [
    { code: true, label: 'Camerounais' },
    { code: false, label: 'Etranger' },
  ]

  etat = [
    { code: 'vivant', label: 'Vivant' },
    { code: 'decede', label: 'Décédé' }
  ];
  filterNoms: Observable<any[]>;
  myControl = new FormControl();

  constructor(
    public dialogRef: MatDialogRef<VictimeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: EnqueteService,
    private messageService: MessageService
  ) { }

  loadAll() {
    this.victime = new Victime();
    this.cuti = JSON.parse(localStorage.getItem('user')).login;
    if(this.data.data != null) this.victime = this.data.data;

    this.victimes = [];
    this.filterNoms = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value),
      map(name => name ? this._filter(name) : this.data.obj.slice())
    );
  }

  displayFn(name: any): string {
    return name ? name : '';
  }
  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.data.obj.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit(): void {
    this.loadAll();
  }

  save() {
    this.victime.nom = this.victime.nom.toUpperCase();
    if(this.victime.prenom == null) this.victime.prenom = '';
    this.victime.prenom = this.victime.prenom.toUpperCase();
    this.victime.cuti = this.cuti;
    this.victime.dateCreation = new Date();
    this.victimes.push(this.victime);
    this.dialogRef.close(this.victimes);

    /*let obj = {
      victimes: this.victimes
    };
    this.service.add(obj, '/victime/savelist')
    .subscribe(
      (res) => this.onSaveSuccess(res),
      (error) => this.onError(error)
    )*/
  }
  onSaveSuccess(res) {
    this.dialogRef.close(res)
  }

  onError(error) {
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  cancel() {
    this.victime = new Victime();
    this.victimes = [];    
    this.dialogRef.close();
  }

  addNew() {
    this.victime.dateCreation = new Date();
    this.victime.cuti = this.cuti;
    this.victimes.push(this.victime);
    this.victime = new Victime();
    console.log(this.victimes);
  }


  onDelete(res) {
    this.messageService.add({severity:'success', summary: 'Information',life:46000, detail: 'Opération effectuée avec succès!!!'});
    this.dialogRef.close(res);
  }

  delete() {
    this.service.delete('/victime/delete/'+this.data.data.id)
    .subscribe(
      (res) => this.onDelete(res),
      (error) => this.onError(error)
    )
  }

}
