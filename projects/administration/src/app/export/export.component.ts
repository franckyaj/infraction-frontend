import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { EnqueteService } from '../enquete.service';
import { Locality, Unite } from '../model/locality';
import * as XLSX from 'xlsx';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { UniteComponent } from '../unite/unite.component';
import { LocaliteComponent } from '../localite/localite.component';


@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  localite: Locality;
  unite:Unite;
  localites: Locality[];
  unites: Unite[];
  type = '';
  routeType: any;
  loaded = false;
  

  constructor(
    private service: EnqueteService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.localite = new Locality();
    this.unite = new Unite();
    this.unites = [];
    this.localites = [];
    this.route.params.subscribe( params => this.initDatas(params.type) );

  }

  initDatas(type) {
    this.routeType = type;
    this.loadDatas(type);
  }

  loadDatas(type) {
    let path = '';
    if(type == 'unite') {
      path = '/localite/unite'
    }
    if(type == 'localite') {
      path = '/localite/list'
    }
    this.spinner.show();
    this.service.listDatas(path)
    .subscribe(
      (res) => this.onLoadSuccess(res),
      (error) => this.onError(error)
    )
  }

  onLoadSuccess(res) {
    this.spinner.hide();

    if(this.routeType == 'unite' ) {
      this.type = 'unite'
      this.unites = res.unites;
      if(this.unites.length > 0)this.loaded = true;
    }
    if(this.routeType == 'localite') {
      this.type = 'localite'
      this.localites = res.localites;
      if(this.localites.length > 0)this.loaded = true;

    }
  }

  onFileChange(ev, type) {
    this.type = type;
    if(type == 'unite') {
      this.unites = [];
      this.onFileChange0(ev);
    }
    else {  
      this.onFileChange1(ev)
    }
  }

  onFileChange1(ev) {
    this.type = 'localite';
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
     // document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
      this.saveData(JSON.parse(dataString).Sheet1);
    }
    reader.readAsBinaryString(file);

  }

  saveData(datas) {
    this.localites = [];
    for(let i of datas) {
      this.localite = new Locality();
      let keys = Object.keys(i);
      let entries = Object.entries(i);
      for(const [prop,  val] of entries ) {
        if(prop == 'REGION') this.localite.region = val;
        if(prop == 'DEPARTEMENT') this.localite.departement = val;
        if(prop == 'ARRONDISSEMENT') this.localite.arrondissement = val;
      }
      this.localites.push(this.localite);
    }
    console.log(this.localites.length);

  }

  saveDatas() {
    let obj = null
    let path = null;
    if(this.type == 'localite') {
      obj = {
        localites: this.localites
      };
      path = '/localite/save';
    }
    else {
      obj = {
        unites: this.unites
      };
      path = '/localite/unite/save';

    }
    this.spinner.show();

    this.service.saveLocalite(obj,path)
    .subscribe(
      (res) => this.onSuccess(res),
      (error) => this.onError(error)
    )
  }

  onSuccess(res) {
    this.spinner.hide();
    this.messageService.add({severity:'success', summary: 'Information',life:296000, detail: 'Sauvegarde effectuée avec succès!!'});
    if(this.type == 'localite') {
      this.localites = res.localites;
    }
    else {
      this.unites = res.unites;
    }

  }

  onError(error) {
    this.spinner.hide();
    this.messageService.add({severity:'error', summary: 'Attention',life:296000, detail: 'Une erreur inattendue est survenue, veuillez réessayer!!'});
  }

  onFileChange0(ev) {
    this.type = 'unite';
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
      this.saveData0(JSON.parse(dataString).Sheet1);
    }
    reader.readAsBinaryString(file);

  }

  saveData0(datas) {
    for(let i of datas) {
      this.unite = new Unite();
      let keys = Object.keys(i);
      let entries = Object.entries(i);
      for(const [prop,  val] of entries ) {
        if(prop == 'Direction de la Police Judiciaire') this.unite.delegation = val;
        if(prop == 'DPJ') this.unite.unite = val;
      }
      this.unites.push(this.unite);
    }

  }

  getRegion(value) {
    let res = null;
    let regions = ['centre', 'adamaoua','nord', 'sud','sud-ouest', 'sud-est', 'nord-ouest','est', 'ouest','littoral',  'extrême-nord']
    for(let i of regions) {
      if(value.toUpperCase().indexOf(i.toUpperCase()) > -1) res = i;
    }
    return res;
  }

  editUnit(entity) {
    const index = this.unites.indexOf(entity);
    let dialogRef = this.dialog.open(UniteComponent, {
      disableClose: false,
      width: '70%',
      data: entity
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null) {
          this.unites[index] = result;
        }
      }
    )
  }

  addUnit() {
    let dialogRef = this.dialog.open(UniteComponent, {
      disableClose: false,
      width: '70%',
      data: null
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null) {
          this.unites.push(result);
        }
      }
    )
  }

  addLocalite() {
    let dialogRef = this.dialog.open(LocaliteComponent, {
      disableClose: false,
      width: '70%',
      data: {obj:null, localite:this.localites}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null) {
          this.localites.push(result);
        }
      }
    )
  }

  editLocalite(entity) {
    const index = this.localites.indexOf(entity);
    let dialogRef = this.dialog.open(LocaliteComponent, {
      disableClose: false,
      width: '70%',
      data: {obj:entity, localite:this.localites}
    })
    dialogRef.afterClosed().subscribe(
      result => {
        console.log(result);
        console.log(index);
        if(result != null) {
          this.localites[index] = result;
        }
      }
    )
  }

  deleteUnit(entity){
    const index = this.unites.indexOf(entity);
    this.unites.splice(index, 1);
  }

  deleteLocalite(entity) {
    const index = this.localites.indexOf(entity);
    this.localites.splice(index, 1);
    console.log(this.localites);
  }

}
