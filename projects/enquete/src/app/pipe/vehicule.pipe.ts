import { Pipe, PipeTransform } from '@angular/core';
import { VehiculeVole } from '../model/enquete';

@Pipe({
  name: 'vehicule'
})
export class VehiculePipe implements PipeTransform {

  transform(value: VehiculeVole[], ...args: unknown[]): unknown {
    let res = '';
    for(let i of value) res = res + i.marque + ';' 
    return res;  }

}
