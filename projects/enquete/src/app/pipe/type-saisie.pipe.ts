import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeSaisie'
})
export class TypeSaisiePipe implements PipeTransform {

  typeSaisie = [
    {code: 'plainte', label: 'Plainte'},
    {code: 'intruction_parquet', label: 'Instruction du parquet'},
    {code: 'comission_derogatoire', label: 'Comission dérogatoire'},
    {code: 'denonciation', label: 'Dénonciation'},
    {code: 'initiative_service', label: 'Initiaive service'},
  ];

  transform(value: unknown, ...args: unknown[]): unknown {
    let res = null;

    res = this.typeSaisie.filter(x => x.code == value)
    return res[0].label;
  }

}
