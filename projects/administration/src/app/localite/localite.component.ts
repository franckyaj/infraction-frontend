import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { EnqueteService } from '../enquete.service';

@Component({
  selector: 'app-localite',
  templateUrl: './localite.component.html',
  styleUrls: ['./localite.component.css']
})
export class LocaliteComponent implements OnInit {

  regions = [];
  departements = [];
  arrondissements = [];
  arrondissement: any;
  region: any;
  departement: any;
  localite = [];
  id: any;

  tmpDepartement: any;
  tmpArrondissement: any;


  constructor(
    public dialogRef: MatDialogRef<LocaliteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private messageService: MessageService,
    private service: EnqueteService
  ) { }

  ngOnInit(): void {
    if(this.data.obj != null) {
      this.tmpDepartement = this.data.obj.departement;
    this.region = this.data.obj.region;
    this.id = this.data.obj.id;
    this.tmpArrondissement = this.data.obj.arrondissement;  
    }
    this.localite = this.data.localite;  
    this.getRegion();
  }

  getRegion() {
    let tmp = [];
    for(let l of this.localite) {
      tmp.push(l.region);
    }
    this.regions = tmp.filter((value, index, categoryArray) => categoryArray.indexOf(value) === index);

    this.getDepartement(this.region, true);
  }

  getDepartement(region,follow?) {
    this.departements = [];
    let tmpDepts = [];
    let r = region;
    if(follow == false) r = region.value;
    let tmp = this.localite.filter(x => x.region == r );
    for(let t of tmp) {
      tmpDepts.push(t.departement);
    }
    this.departements = tmpDepts.filter((value, index, categoryArray) => categoryArray.indexOf(value) === index);
    this.departements.push('AUTRE');
    if(follow == true) this.getArrondissement(this.tmpDepartement, true);
  }

  getArrondissement(departement,follow?) {
    this.arrondissements = [];
    let d = departement;
    if(follow == false) d = departement.value;
    let tmpArr = [];
    let tmp = this.localite.filter(x => x.departement == d );
    for(let t of tmp) {
      tmpArr.push(t.arrondissement);
    }
    this.arrondissements = tmpArr.filter((value, index, categoryArray) => categoryArray.indexOf(value) === index);
    this.arrondissements.push('AUTRE');
  }

  save() {
    if(this.departement == null) this.departement = this.tmpDepartement;
    if(this.arrondissement == null) this.arrondissement = this.tmpArrondissement;

    let obj = {
      region: this.region,
      departement: this.departement,
      arrondissement: this.arrondissement,
      id: this.id
    }
    this.dialogRef.close(obj);
  }

  cancel() {
    this.dialogRef.close(null);
  }

}
