import { Injectable } from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  constructor() { }

  //generation du document PDF
  generatePdf(headers, datas, title, ttile_stat){

    let body = [];
    let headersCopy = [];
    let format = 'landscape'
    let width = [];
    console.log('0');
    //ajout du cess bold sur les headers
    for(let i of headers) {
      headersCopy.push(
        {text: i, bold:true}
      )
    }
    console.log('1');
    //construction du contenu de la table: header  + data
    body.push(headersCopy);
    for(let i of datas) {
      body.push(i);
    }
    

    //definition du width de chaque colonne
    for(let i=0;  i<headers.length; i++) {
        width.push("auto");
    }
    console.log('2');
    //definition du mode d'affichage de PDF
    if(headers.length <= 5) {
      for(let i=0;  i<headers.length; i++) {
        width.push(200);
        format = 'portrait'
    }

    }
    console.log('3 ' + title);
    //creation du document
    const documentDefinition =  {
      //pagination
      footer: function(currentPage, pageCount) { return { text: currentPage.toString(), alignment: 'center' }  },
      pageOrientation: format, //mode d'affichage
      content: [
        {text: ttile_stat, style: "tableHeader" ,fontSize:14, alignment: "center"},
        {text: " ", style: "tableHeader" , alignment: "center"},
        {
          layout: 'lightHorizontalLines', // optional``
          
          columns: [
            { width: '*', text: '' },
            {
                width: 'auto',
                table: {
                  headerRows: 1,
                  body: body,
                  alignment: "center"
                }
            },
            { width: '*', text: '' },
          ]
        }
      ],
      styles: {
        tableHeader:{
          bold: true,
          color: 'green',
          decoration: 'underline'
        }
      },
      defaultStyle: { // style par defaut
        fontSize: 9,
        bold: false
      }
    
    };
    pdfMake.createPdf(documentDefinition).open();
  }
}
