export class Locality {

    region: any;
    departement: any;
    arrondissement: any;
    unite: String;
}

export class Unite {

    delegation: any;
    unite: any;
}

export class ParamInfraction {
    categorieInfraction: any;
    designationInfraction: any;
    posteInfraction: any;
    active: boolean;

}
